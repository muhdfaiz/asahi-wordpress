<?php get_header(); ?>

<?php
$fields = get_field_objects();
$banner = reset($fields['banner']['value'])
?>

    <main>
        <header>
            <div class="brandslider generalslider">
                <div class="brandslider-item bannerpresence d-flex align-content-end flex-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 d-lg-none brandslider-placeholder"></div>
                            <div class="col-12 col-lg-8 brandslider-content">
                                <?php if (isset($banner['title']) && $banner['title'] && $banner['title'] !== '#') { ?>
                                    <h2 class="text-yellow mb-0 text-capitalize"><?php echo $banner['title'] ?></h2>
                                <?php } ?>

                                <?php if (isset($banner['description']) && $banner['description'] && $banner['description'] !== '#') { ?>
                                    <p class="text-white bannertxt"><?php echo $banner['description'] ?></p>
                                <?php } ?>

                                <?php if ($banner['button_name'] && $banner['button_name'] !== '#') { ?>
                                    <a href="<?php echo $banner['button_url'] ?: '#' ?>" class="bevelcorner-solid largerpad minwidth mb-4">
                                        <div class="bevelcorner__inner"><?php echo $banner['button_name'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="bg-white sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-9">
                        <?php if (isset($fields['our_presence_page_title']) && $fields['our_presence_page_title']) { ?>
                            <p class="text-tiff"><strong><?php echo $fields['our_presence_page_title']['value'] ?></strong></p>
                        <?php } ?>

                        <?php if (isset($fields['our_presence_page_description']) && $fields['our_presence_page_description']) { ?>
                            <p class="sub"><strong><?php echo $fields['our_presence_page_description']['value'] ?></strong></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>



        <section class="bg-internationalexports sect-deskspacer d-flex align-content-lg-center flex-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-none px-0">
                        <?php if (isset($fields['our_presence_page_regional_and_international_exports_background_mobile']) && $fields['our_presence_page_regional_and_international_exports_background_mobile']) { ?>
                            <img src="<?php echo $fields['our_presence_page_regional_and_international_exports_background_mobile']['value']['url'] ?>" class="d-md-none w-100" alt="Regional & International Exports image">
                        <?php } ?>
                    </div>
                    <div class="col-12 col-lg-8 pt-4 pb-3 pt-lg-0 my-auto">
                        <?php if (isset($fields['our_presence_page_regional_and_international_exports_title']) && $fields['our_presence_page_regional_and_international_exports_title']) { ?>
                            <h3 class="text-yellow text-capitalize mt-3"><?php echo $fields['our_presence_page_regional_and_international_exports_title']['value'] ?></h3>
                        <?php } ?>
                    </div>
                    <div class="col-12 col-lg-7 pr-lg-5 pb-5 pb-lg-0 my-auto">
                        <?php if (isset($fields['our_presence_page_regional_and_international_exports_description']) && $fields['our_presence_page_regional_and_international_exports_description']) { ?>
                            <p class="text-white sub mb-4 pb-lg-2"><?php echo $fields['our_presence_page_regional_and_international_exports_description']['value'] ?></p>
                        <?php } ?>

                        <?php if (isset($fields['our_presence_page_regional_and_international_exports_button_url']) && $fields['our_presence_page_regional_and_international_exports_button_url']) { ?>
                            <a href="<?php echo $fields['our_presence_page_regional_and_international_exports_button_url']['value'] ?>" target="_blank" class="bevelcorner-solid minwidthlg mb-2">
                                <?php if (isset($fields['our_presence_page_regional_and_international_exports_button_name']) && $fields['our_presence_page_regional_and_international_exports_button_name']) { ?>
                                    <div class="bevelcorner__inner"><?php echo $fields['our_presence_page_regional_and_international_exports_button_name']['value'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                <?php } ?>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <script>
		document.head.insertAdjacentHTML("beforeend", `<style>
        header .mainslider-item.bannerpresence, header .brandslider-item.bannerpresence {
            background-image: url(<?php echo $banner['mobile_banner']['url'] ?>);
            background-position: center;
        }
        @media (min-width: 992px) {
            header .mainslider-item.bannerpresence, header .brandslider-item.bannerpresence {
                background-image: url(<?php echo $banner['desktop_banner']['url'] ?>);
                background-position: center;
            }
        }
        @media all and (min-width: 767px) and (max-width: 991.98px) {
            header .mainslider-item.bannerpresence, header .brandslider-item.bannerpresence {
                background-image: url(<?php echo $banner['tablet_banner']['url'] ?>);
            }
        }
        <?php if (isset($fields['our_presence_page_regional_and_international_exports_background_desktop']) && $fields['our_presence_page_regional_and_international_exports_background_desktop']) { ?>
            @media (min-width: 768px) {
                .bg-internationalexports {
                    background-image: url(<?php echo $fields['our_presence_page_regional_and_international_exports_background_desktop']['value']['url'] ?>);
                    background-size: cover;
                }
            }
        <?php } ?>
    </style>`)
    </script>
<?php get_footer(); ?>
