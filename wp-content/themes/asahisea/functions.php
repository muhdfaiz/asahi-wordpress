<?php

if ( ! function_exists( 'asahi_sea_setup' ) ) {
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @since Twenty Twenty-One 1.0
     *
     * @return void
     */
    function asahi_sea_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Twenty Twenty-One, use a find and replace
         * to change 'twentytwentyone' to the name of your theme in all the template files.
         */
        load_theme_textdomain( 'asahisea', get_template_directory() . '/languages' );

        /*
         * Let WordPress manage the document title.
         * This theme does not use a hard-coded <title> tag in the document head,
         * WordPress will provide it for us.
         */
        add_theme_support( 'title-tag' );

        /**
         * Add post-formats support.
         */
        add_theme_support(
            'post-formats',
            array(
                'link',
                'aside',
                'gallery',
                'image',
                'quote',
                'status',
                'video',
                'audio',
                'chat',
            )
        );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 1568, 9999 );

        register_nav_menus(
            array(
                'primary' => esc_html__( 'Primary menu', 'twentytwentyone' ),
                'footer'  => __( 'Secondary menu', 'twentytwentyone' ),
            )
        );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5',
            array(
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'style',
                'script',
                'navigation-widgets',
            )
        );

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        $logo_width  = 300;
        $logo_height = 100;

        add_theme_support(
            'custom-logo',
            array(
                'height'               => $logo_height,
                'width'                => $logo_width,
                'flex-width'           => true,
                'flex-height'          => true,
                'unlink-homepage-logo' => true,
            )
        );

        // Add theme support for selective refresh for widgets.
        add_theme_support( 'customize-selective-refresh-widgets' );

        // Add support for Block Styles.
        add_theme_support( 'wp-block-styles' );

        // Add support for full and wide align images.
        add_theme_support( 'align-wide' );

        // Add support for editor styles.
        add_theme_support( 'editor-styles' );

        $editor_stylesheet_path = './assets/css/style-editor.css';

        // Note, the is_IE global variable is defined by WordPress and is used
        // to detect if the current browser is internet explorer.
        global $is_IE;
        if ( $is_IE ) {
            $editor_stylesheet_path = './assets/css/ie-editor.css';
        }

        // Enqueue editor styles.
        add_editor_style( $editor_stylesheet_path );

        // Editor color palette.
        $black     = '#000000';
        $dark_gray = '#28303D';
        $gray      = '#39414D';
        $green     = '#D1E4DD';
        $blue      = '#D1DFE4';
        $purple    = '#D1D1E4';
        $red       = '#E4D1D1';
        $orange    = '#E4DAD1';
        $yellow    = '#EEEADD';
        $white     = '#FFFFFF';

        add_theme_support(
            'editor-color-palette',
            array(
                array(
                    'name'  => esc_html__( 'Black', 'twentytwentyone' ),
                    'slug'  => 'black',
                    'color' => $black,
                ),
                array(
                    'name'  => esc_html__( 'Dark gray', 'twentytwentyone' ),
                    'slug'  => 'dark-gray',
                    'color' => $dark_gray,
                ),
                array(
                    'name'  => esc_html__( 'Gray', 'twentytwentyone' ),
                    'slug'  => 'gray',
                    'color' => $gray,
                ),
                array(
                    'name'  => esc_html__( 'Green', 'twentytwentyone' ),
                    'slug'  => 'green',
                    'color' => $green,
                ),
                array(
                    'name'  => esc_html__( 'Blue', 'twentytwentyone' ),
                    'slug'  => 'blue',
                    'color' => $blue,
                ),
                array(
                    'name'  => esc_html__( 'Purple', 'twentytwentyone' ),
                    'slug'  => 'purple',
                    'color' => $purple,
                ),
                array(
                    'name'  => esc_html__( 'Red', 'twentytwentyone' ),
                    'slug'  => 'red',
                    'color' => $red,
                ),
                array(
                    'name'  => esc_html__( 'Orange', 'twentytwentyone' ),
                    'slug'  => 'orange',
                    'color' => $orange,
                ),
                array(
                    'name'  => esc_html__( 'Yellow', 'twentytwentyone' ),
                    'slug'  => 'yellow',
                    'color' => $yellow,
                ),
                array(
                    'name'  => esc_html__( 'White', 'twentytwentyone' ),
                    'slug'  => 'white',
                    'color' => $white,
                ),
            )
        );

        add_theme_support(
            'editor-gradient-presets',
            array(
                array(
                    'name'     => esc_html__( 'Purple to yellow', 'twentytwentyone' ),
                    'gradient' => 'linear-gradient(160deg, ' . $purple . ' 0%, ' . $yellow . ' 100%)',
                    'slug'     => 'purple-to-yellow',
                ),
                array(
                    'name'     => esc_html__( 'Yellow to purple', 'twentytwentyone' ),
                    'gradient' => 'linear-gradient(160deg, ' . $yellow . ' 0%, ' . $purple . ' 100%)',
                    'slug'     => 'yellow-to-purple',
                ),
                array(
                    'name'     => esc_html__( 'Green to yellow', 'twentytwentyone' ),
                    'gradient' => 'linear-gradient(160deg, ' . $green . ' 0%, ' . $yellow . ' 100%)',
                    'slug'     => 'green-to-yellow',
                ),
                array(
                    'name'     => esc_html__( 'Yellow to green', 'twentytwentyone' ),
                    'gradient' => 'linear-gradient(160deg, ' . $yellow . ' 0%, ' . $green . ' 100%)',
                    'slug'     => 'yellow-to-green',
                ),
                array(
                    'name'     => esc_html__( 'Red to yellow', 'twentytwentyone' ),
                    'gradient' => 'linear-gradient(160deg, ' . $red . ' 0%, ' . $yellow . ' 100%)',
                    'slug'     => 'red-to-yellow',
                ),
                array(
                    'name'     => esc_html__( 'Yellow to red', 'twentytwentyone' ),
                    'gradient' => 'linear-gradient(160deg, ' . $yellow . ' 0%, ' . $red . ' 100%)',
                    'slug'     => 'yellow-to-red',
                ),
                array(
                    'name'     => esc_html__( 'Purple to red', 'twentytwentyone' ),
                    'gradient' => 'linear-gradient(160deg, ' . $purple . ' 0%, ' . $red . ' 100%)',
                    'slug'     => 'purple-to-red',
                ),
                array(
                    'name'     => esc_html__( 'Red to purple', 'twentytwentyone' ),
                    'gradient' => 'linear-gradient(160deg, ' . $red . ' 0%, ' . $purple . ' 100%)',
                    'slug'     => 'red-to-purple',
                ),
            )
        );

        // Add support for responsive embedded content.
        add_theme_support( 'responsive-embeds' );

        // Add support for custom line height controls.
        add_theme_support( 'custom-line-height' );

        // Add support for experimental link color control.
        add_theme_support( 'experimental-link-color' );

        // Add support for experimental cover block spacing.
        add_theme_support( 'custom-spacing' );

        // Add support for custom units.
        // This was removed in WordPress 5.6 but is still required to properly support WP 5.5.
        add_theme_support( 'custom-units' );
    }
}

add_action( 'after_setup_theme', 'asahi_sea_setup' );

/**
 * Register Header Menu Walker
 */
function register_header_menu_navwalker(){
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}

add_action( 'after_setup_theme', 'register_header_menu_navwalker' );

/**
 * Asahi SEA Menus
 */
function asahi_sea_menus() {
    register_nav_menus(array(
        'header-menu' => 'Header Menu',
        'about-us-footer-menu' => 'About Us Footer Menu',
        'our-brands-footer-menu' => 'Our Brand Footer Menu',
        'sustainability-footer-menu' => 'Sustainability Footer Menu',
        'news-footer-menu' => 'News Footer Menu',
        'careers-footer-menu' => 'Careers Footer Menu',
        'contact-us-footer-menu' => 'Contact Us Footer Menu',
    ));
}

add_action('init', 'asahi_sea_menus');

function add_additional_class_on_li($classes, $item, $args) {
    if(isset($args->add_li_class)) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);


function asahi_sea_scripts() {
    wp_enqueue_style('bootstrap4', get_template_directory_uri() . '/vendor/bootstrap/bootstrap.min.css', array(), '1.0.0', 'screen');
    wp_enqueue_style('style', get_template_directory_uri() . '/css/style.css');
    wp_enqueue_style('slick', get_template_directory_uri() . '/vendor/slick/slick.css');
    wp_enqueue_style('slick-theme', get_template_directory_uri() . '/vendor/slick/slick-theme.css');
    wp_enqueue_script( 'js1',get_template_directory_uri() . '/vendor/jquery/jquery.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'js2',get_template_directory_uri() . '/vendor/bootstrap/bootstrap.bundle.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'js3',get_template_directory_uri() . '/vendor/slick/slick.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'js5',get_template_directory_uri() . '/js/affix.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'js6',get_template_directory_uri() . '/js/style.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'js7','https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.js', array( 'jquery' ),'',true );
}

add_action( 'wp_enqueue_scripts', 'asahi_sea_scripts' );

/* Disable WordPress Admin Bar for all users */
add_filter( 'show_admin_bar', '__return_false' );

if ( function_exists( 'get_field' ) ) {
    function get_group_field( string $group, string $field, $post_id = 0 ) {
        $group_data = get_field( $group, $post_id );
        if ( is_array( $group_data ) && array_key_exists( $field, $group_data ) ) {
            return $group_data[ $field ];
        }
        return null;
    }
}

/*removing default submit tag*/
remove_action('wpcf7_init', 'wpcf7_add_form_tag_submit');

/*adding action with function which handles our button markup*/
add_action('wpcf7_init', 'asahisea_child_cf7_button');

/*adding out submit button tag*/
if (!function_exists('asahisea_cf7_button')) {
    function asahisea_child_cf7_button() {
        wpcf7_add_form_tag('submit', 'asahisea_child_cf7_button_handler');
    }
}


/*out button markup inside handler*/
if (!function_exists('asahisea_child_cf7_button_handler')) {
    function asahisea_child_cf7_button_handler($tag) {
        return '<button type="submit" id="submitContactUs" class="wpcf7-form-control wpcf7-submit bevelcorner-solid largerpad minwidth" value=""><div class="bevelcorner__inner">Submit<ion-icon name="chevron-forward-sharp"></ion-icon></div> </button>';
    }
}

add_action( 'wp_footer', 'asahisea_wp_footer' );

function asahisea_wp_footer() { ?>
    <script type="text/javascript">
		document.addEventListener( 'wpcf7mailsent', function( event ) {
			if (702 == event.detail.contactFormId ) {
				$('#modalthankyou').modal('show');
				//window.location.reload = '/contact-us#thank-you'
			}
		}, false );
    </script>
<?php  } ?>

<?php

// Add Footer Options menu in sidebar inside admin dashboard.
add_action("admin_menu", "asahisea_site_settings");

function asahisea_site_settings() {
    add_menu_page('Site Settings', "Site Settings", "manage_options", "site-settings", "site_options", "dashicons-admin-tools");
}

function site_options() { ?>
    <div class="wrap">
        <h1 class="wp-heading-inline">Site Settings</h1>

        <div style="margin: 10px 0 10px; border: 1px solid #ccc; background: #fff; padding: 15px 15px; box-sizing: border-box;">
            <div class="display-setup">
                <span>Here you can update available theme settings.</span>
            </div>
        </div>
        <span>
            <?php settings_errors(); ?>
        </span>
        <form action="options.php" method="post">
            <?php
                settings_fields("site-option-section");
                do_settings_sections("site-options");
                submit_button();
            ?>
        </form>
    </div>
<?php }


function site_options_settings() {
    add_settings_section("site-option-section", "", null, "site-options");

    add_settings_field("meta_description", "Meta Description", "display_meta_description", "site-options", "site-option-section");

    add_settings_field("gtm_id", "Google Tag Manager (GTM) ID", "display_gtm_id", "site-options", "site-option-section");

    add_settings_field("facebook_og_title", "Facebook OG Title", "display_facebook_og_title", "site-options", "site-option-section");

    add_settings_field("facebook_og_description", "Facebook OG Description", "display_facebook_og_description", "site-options", "site-option-section");

    add_settings_field("facebook_og_image", "Facebook OG Image", "display_facebook_og_image", "site-options", "site-option-section");

    add_settings_field("copyright_text", "Copyright Text", "display_copyright_text", "site-options", "site-option-section");

    add_settings_field("privacy_policy", "Privacy Policy File", "display_privacy_policy", "site-options", "site-option-section");

    register_setting("site-option-section", "meta_description");

    register_setting("site-option-section", "gtm_id");

    register_setting("site-option-section", "facebook_og_title");

    register_setting("site-option-section", "facebook_og_description");

    register_setting("site-option-section", "facebook_og_image");

    register_setting("site-option-section", "copyright_text");

    register_setting("site-option-section", "privacy_policy");
}

add_action("admin_init", "site_options_settings");

function display_meta_description() { ?>
    <textarea rows="4" name="meta_description" class="regular-text" id="meta_description"><?php echo get_option('meta_description'); ?></textarea>
<?php }

function display_gtm_id() { ?>
    <input type="text" name="gtm_id" class="regular-text" value="<?php echo get_option('gtm_id'); ?>" id="gtm_id">
<?php }

function display_facebook_og_title() { ?>
    <input type="text" name="facebook_og_title" class="regular-text" value="<?php echo get_option('facebook_og_title'); ?>" id="facebook_og_title">
<?php }

function display_facebook_og_description() { ?>
    <textarea rows="4" name="facebook_og_description" class="regular-text" id="facebook_og_description"><?php echo get_option('facebook_og_description'); ?></textarea>
<?php }

function display_facebook_og_image() { ?>
    <?php if (get_option('facebook_og_image')) { ?>
        <div id="facebook-og-mage-thumbnail" style="margin-bottom: 10px; width: 10em">
            <div style="text-align: center">
                <img src="<?php echo get_option('facebook_og_image'); ?>" class="icon">
            </div>
        </div>
    <?php } else { ?>
        <div style="margin-bottom: 5px; width: 10em" id="no-facebook-og-image">No image selected</div>
        <div id="facebook-og-image-thumbnail" style="margin-bottom: 10px; width: 10em; display: none;">
            <div style="text-align: center">
                <img src="" class="icon">
            </div>
        </div>
    <?php } ?>
    <input id="upload_facebook_og_image_button" class="button" type="button" value="Upload Facebook OG Image" />
    <input id="facebook_og_image" type="hidden" name="facebook_og_image" value="" />
<?php }

function display_copyright_text() { ?>
    <input type="text" name="copyright_text" class="regular-text" value="<?php echo get_option('copyright_text'); ?>" id="copyright_text">
<?php }

function display_privacy_policy() { ?>
    <?php if (get_option('privacy_policy')) { ?>
        <a target="_blank" href="<?php echo get_option('privacy_policy'); ?>">
            <div style="margin-bottom: 10px; width: 10em">
                <div style="text-align: center">
                    <img src="/wp-includes/images/media/document.png" class="icon" draggable="false" alt="">
                </div>
                <div style="text-align: center">privacy_policy.pdf</div>
            </div>
        </a>
    <?php } else { ?>
        <div style="margin-bottom: 5px; width: 10em" id="no-file">No file selected</div>
        <a id="privacy-policy-link" target="_blank" href="" style="display: none;">
            <div style="margin-bottom: 10px; width: 10em">
                <div style="text-align: center">
                    <img src="/wp-includes/images/media/document.png" class="icon">
                </div>
                <div id="filename" style="text-align: center"></div>
            </div>
        </a>
    <?php } ?>
    <input id="upload_privacy_policy_button" class="button" type="button" value="Upload Privacy Policy" />
    <input id="privacy_policy" type="hidden" name="privacy_policy" value="<?php echo get_option('privacy_policy'); ?>" />
<?php }

function admin_scripts() {
    wp_enqueue_media();
    wp_register_script('admin-js', get_template_directory_uri() . '/js/admin.js', array('jquery'));
    wp_enqueue_script('admin-js');
}

add_action( 'admin_enqueue_scripts', 'admin_scripts' );
?>
