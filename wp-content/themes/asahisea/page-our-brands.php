<?php get_header(); ?>

<?php
$fields = get_field_objects();
$banner = reset($fields['banner']['value'])
?>
    <main>
        <header>
            <div class="brandslider generalslider">
                <div class="brandslider-item bannerourbrands d-flex align-content-end flex-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 d-lg-none brandslider-placeholder"></div>
                            <div class="col-12 col-lg-9 brandslider-content">
                                <?php if (isset($banner['title']) && $banner['title'] && $banner['title'] !== '#') { ?>
                                    <h2 class="text-yellow mb-0 text-capitalize"><?php echo $banner['title'] ?></h2>
                                <?php } ?>

                                <?php if (isset($banner['description']) && $banner['description'] && $banner['description'] !== '#') { ?>
                                    <p class="text-white bannertxt"><?php echo $banner['description'] ?></p>
                                <?php } ?>

                                <?php if ($banner['button_name'] && $banner['button_name'] !== '#') { ?>
                                    <a href="<?php echo $banner['button_url'] ?: '#' ?>" class="bevelcorner-solid largerpad minwidth mb-4">
                                        <div class="bevelcorner__inner"><?php echo $banner['button_name'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="bg-white sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <?php if (isset($fields['our_brands_page_title']) && $fields['our_brands_page_title']) { ?>
                            <p class="text-tiff"><strong><?php echo $fields['our_brands_page_title']['value'] ?></strong></p>
                        <?php } ?>

                        <?php if (isset($fields['our_brands_page_description']) && $fields['our_brands_page_description']) { ?>
                            <p class="sub"><?php echo $fields['our_brands_page_description']['value'] ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="bg-blue sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-3 mb-lg-5">
                        <?php if (isset($fields['our_brands_page_main_brands_title']) && $fields['our_brands_page_main_brands_title']) { ?>
                            <h3 class="text-white text-capitalize"><?php echo $fields['our_brands_page_main_brands_title']['value'] ?></h3>
                        <?php } ?>
                    </div>

                    <?php if (isset($fields['our_brands_page_main_brands']) && $fields['our_brands_page_main_brands']) { ?>
                        <?php foreach ($fields['our_brands_page_main_brands']['value'] as $item) { ?>
                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="brands-logoimg largerwidth position-relative">
                                    <img src="<?php echo $item['logo']['url']; ?>" alt="<?php echo $item['logo']['alt']; ?>">
                                    <a href="<?php echo isset($item['link']) && $item['link'] ? $item['link'] : '#' ?>" class="stretched-link"></a>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>

        <section class="bg-blue-sky sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-4 mb-lg-4">
                        <?php if (isset($fields['our_brands_page_other_brands_title']) && $fields['our_brands_page_other_brands_title']) { ?>
                            <h3 class="text-blue text-capitalize"><?php echo $fields['our_brands_page_other_brands_title']['value'] ?></h3>
                        <?php } ?>
                    </div>


                    <div class="col-12 p-xs-0">
                        <div class="row no-xs-gutters ourbrands2row">
                            <?php if (isset($fields['our_brands_page_other_brands']) && $fields['our_brands_page_other_brands']) { ?>
                                <?php foreach ($fields['our_brands_page_other_brands']['value'] as $item) { ?>
                                    <div class="col-12 col-lg-3 mb-4 mb-lg-0">
                                        <div class="brands-logoimg largerwidth position-relative">
                                            <img src="<?php echo $item['logo']['url']; ?>" alt="<?php echo $item['logo']['alt']; ?>">
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bg-light sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-4 mb-lg-4">
                        <?php if (isset($fields['our_brands_page_franchise_brands_title']) && $fields['our_brands_page_franchise_brands_title']) { ?>
                            <h3 class="text-blue text-capitalize"><?php echo $fields['our_brands_page_franchise_brands_title']['value'] ?></h3>
                        <?php } ?>
                    </div>

                    <div class="col-12 p-xs-0">
                        <div class="row no-xs-gutters ourbrands2row">
                            <?php if (isset($fields['our_brands_page_franchise_brands']) && $fields['our_brands_page_franchise_brands']) { ?>
                                <?php foreach ($fields['our_brands_page_franchise_brands']['value'] as $item) { ?>
                                    <div class="col-12 col-lg-s5 mb-5 mb-lg-0">
                                        <div class="brands-logoimg largerwidth position-relative">
                                            <img src="<?php echo $item['logo']['url']; ?>" alt="<?php echo $item['logo']['alt']; ?>">
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="col-12 mt-4">
                        <?php if (isset($fields['our_brands_page_franchise_brands_note_1']) && $fields['our_brands_page_franchise_brands_note_1']) { ?>
                            <p class="suptiny mb-lg-0"><?php echo $fields['our_brands_page_franchise_brands_note_1']['value'] ?></p>
                        <?php } ?>

                        <?php if (isset($fields['our_brands_page_franchise_brands_note_2']) && $fields['our_brands_page_franchise_brands_note_2']) { ?>
                            <p class="suptiny"><?php echo $fields['our_brands_page_franchise_brands_note_2']['value'] ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <script>
		document.head.insertAdjacentHTML("beforeend", `<style>
        header .mainslider-item.bannerourbrands, header .brandslider-item.bannerourbrands {
            background-image: url(<?php echo $banner['mobile_banner']['url'] ?>);
            background-position: center;
        }
        @media (min-width: 992px) {
            header .mainslider-item.bannerourbrands, header .brandslider-item.bannerourbrands {
                background-image: url(<?php echo $banner['desktop_banner']['url'] ?>);
                background-position: center;
            }
        }
        @media all and (min-width: 767px) and (max-width: 991.98px) {
            header .mainslider-item.bannerourbrands, header .brandslider-item.bannerourbrands {
                background-image: url(<?php echo $banner['tablet_banner']['url'] ?>);
            }
        }
    </style>`)
    </script>
<?php get_footer(); ?>
