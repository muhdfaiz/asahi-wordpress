//Menu On Hover
$('body').on('mouseenter mouseleave','.nav-item',function(e){
  if ($(window).width() > 750) {
    var _d=$(e.target).closest('.nav-item');_d.addClass('show');
    setTimeout(function(){
    _d[_d.is(':hover')?'addClass':'removeClass']('show');
    },1);
  }
});	


$(document).click(function (event) {
  var clickover = $(event.target);
  var $navbar = $(".navbar-collapse");
  var _opened = $navbar.hasClass("show");
  if (_opened === true && !clickover.hasClass("navbar-toggle")) {
      $navbar.collapse('hide');
  }
});



$('.brandslider').slick({
  dots: true,
  arrows: false,
  infinite: true,
  pauseOnHover: false,
  pauseOnFocus: false,
  pauseOnDotsHover: false,
  autoplay: true,
  autoplaySpeed: 3500,
  speed: 800,
  slidesToShow: 1
})
.on('setPosition', function (event, slick) {
	slick.$slides.css('height', slick.$slideTrack.height() + 'px');
});

$('.mainslider').slick({
  dots: true,
  arrows: false,
  infinite: true,
  pauseOnHover: false,
  pauseOnFocus: false,
  pauseOnDotsHover: false,
  autoplay: true,
  autoplaySpeed: 3500,
  speed: 800,
  slidesToShow: 1
});


// $('.titlebrandslider').slick({
//   dots: false,
//   arrows: true,
//   infinite: true,
//   autoplay: false,
//   speed: 800,
//   slidesToShow: 3,
//   slidesToScroll: 1
// });

$('.productnavslider-1').slick({
  dots: false,
	arrows: true,
  infinite: false,
  speed: 200,
  slidesToShow: 3,
  slidesToScroll: 1,
  accessibility: false,
  draggable: false,
  focusOnSelect: false,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        centerMode: true,
        centerPadding: 0,
        fade: true,
        asNavFor: '.productcontentslider-1'
      }
    }
  ]
});

$('.productcontentslider-1').slick({
  dots: false,
  arrows: false,
  infinite: false,
  autoplay: false,
  speed: 200,
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
  accessibility: false,
  draggable: false,
  focusOnSelect: false,
  draggable: false,
  swipe: false,
  swipeToSlide: false,
  touchMove: false,

  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        centerMode: true,
        centerPadding: 0,
        fade: true,
        asNavFor: '.productnavslider-1'
      }
    }
  ]
});
$('.productnavslider-1 [data-slide]').click(function(e) {
  e.preventDefault();
  var slideno = $(this).data('slide');
  $('.productcontentslider-1').slick('slickGoTo', slideno - 1);
});
$(document).on("click", ".productnavslider-1 .titlecover", function() {
  if($(this).hasClass("active")) {
    $(this).addClass("active");
  }
  else {
    $('.productnavslider-1 .titlecover').removeClass("active");
    $(this).addClass("active");
  }
});






$('.productnavslider-2').slick({
  dots: false,
	arrows: true,
  infinite: false,
  speed: 200,
  slidesToShow: 3,
  slidesToScroll: 1,
  accessibility: false,
  draggable: false,
  focusOnSelect: false,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        centerMode: true,
        centerPadding: 0,
        fade: true,
        swipeToSlide: false,
        asNavFor: '.productcontentslider-2'
      }
    }
  ]
});

$('.productcontentslider-2').slick({
  dots: false,
  arrows: false,
  infinite: false,
  autoplay: false,
  speed: 200,
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
  accessibility: false,

  focusOnSelect: false,
  draggable: false,
  swipe: false,
  swipeToSlide: false,
  touchMove: false,

  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        centerMode: true,
        centerPadding: 0,
        fade: true,
        asNavFor: '.productnavslider-2'
      }
    }
  ]
});
$('.productcontentslider-2').slick("slickSetOption", "draggable", false, false);

$('.productnavslider-2 [data-slide]').click(function(e) {
  e.preventDefault();
  var slideno = $(this).data('slide');
  $('.productcontentslider-2').slick('slickGoTo', slideno - 1);
});
$(document).on("click", ".productnavslider-2 .titlecover", function() {
  if($(this).hasClass("active")) {
    $(this).addClass("active");
  }
  else {
    $('.productnavslider-2 .titlecover').removeClass("active");
    $(this).addClass("active");
  }
});




$('.crossellslider').slick({
  dots: false,
	arrows: true,
  infinite: false,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 767,
      settings: {
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
});



$('.ourbrands').slick({
  dots: false,
	arrows: false,
  infinite: false,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
});


$('.ourbrands2row').slick({
  dots: true,
	arrows: false,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  mobileFirst: true,
  responsive: [
    {
      breakpoint: 993,
      settings: 'unslick'
    },
    {
      breakpoint: 560,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    }
  ]
});


$('.slide5items').slick({
  dots: true,
	arrows: false,
  infinite: false,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
});


$('.slide4items').slick({
  dots: true,
	arrows: false,
  infinite: false,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
});


$(window).on('resize', function() {
  $('.generalslider').slick('refresh');
  $('.ourbrands2row').slick('resize');
});

// Display contact form success modal.
if (window.location.hash === '#thank-you') {
  $('#modalthankyou').modal('show'); //this is the bootstrap modal popup id
}
