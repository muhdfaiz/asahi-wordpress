jQuery(document).ready(function($){
	var privacyPolicyUploader;

	$('#upload_privacy_policy_button').click(function(e) {
		e.preventDefault();
		//If the uploader object has already been created, reopen the dialog
		if (privacyPolicyUploader) {
			privacyPolicyUploader.open();
			return;
		}
		//Extend the wp.media object
		privacyPolicyUploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose File',
			button: {
				text: 'Choose File'
			},
			multiple: false,
			library: {
				type: [ 'application/pdf' ]
			},
		});
		//When a file is selected, grab the URL and set it as the text field's value
		privacyPolicyUploader.on('select', function() {
			attachment = privacyPolicyUploader.state().get('selection').first().toJSON();
			$('#no-file').hide();
			$('#filename').text(attachment.filename);
			$('#privacy-policy-link').attr('href', attachment.url).show();
			$('#privacy_policy').val(attachment.url);
		});
		//Open the privacyPolicyUploader dialog
		privacyPolicyUploader.open();
	});

	var facebookOGImageUploader;

	$('#upload_facebook_og_image_button').click(function(e) {
		e.preventDefault();
		//If the uploader object has already been created, reopen the dialog
		if (facebookOGImageUploader) {
			facebookOGImageUploader.open();
			return;
		}
		//Extend the wp.media object
		facebookOGImageUploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose File',
			button: {
				text: 'Choose File'
			},
			multiple: false,
			library: {
				type: [ 'image' ]
			},
		});
		//When a file is selected, grab the URL and set it as the text field's value
		facebookOGImageUploader.on('select', function() {
			attachment = facebookOGImageUploader.state().get('selection').first().toJSON();
			$('#no-facebook-og-image').hide();
			$('#facebook-og-image-thumbnail').show();
			$('#facebook-og-image-thumbnail img').attr('src', attachment.url)
			$('#facebook_og_image').val(attachment.url);
		});
		//Open the uploader dialog
		facebookOGImageUploader.open();
	});
});