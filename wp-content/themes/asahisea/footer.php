<?php
    $theme_locations = get_nav_menu_locations();

    $about_us_menu = get_term( $theme_locations['about-us-footer-menu'], 'nav_menu' );

    $about_us_sub_menus = [];

    if (!is_wp_error($about_us_menu)) {
        $about_us_sub_menus = wp_get_nav_menu_items($about_us_menu->term_id);
    }

    $our_brands_menu = get_term( $theme_locations['our-brands-footer-menu'], 'nav_menu' );

    $our_brands_sub_menus = [];

    if (!is_wp_error($our_brands_menu)) {
        $our_brands_sub_menus = wp_get_nav_menu_items($our_brands_menu->term_id);
    }

    $sustainability_menu = get_term( $theme_locations['sustainability-footer-menu'], 'nav_menu' );

    $sustainability_sub_menus = [];

    if (!is_wp_error($sustainability_menu)) {
        $sustainability_sub_menus = wp_get_nav_menu_items($sustainability_menu->term_id);
    }

    $news_menu = get_term( $theme_locations['news-footer-menu'], 'nav_menu' );

    $news_sub_menus = [];

    if (!is_wp_error($news_menu)) {
        $news_sub_menus = wp_get_nav_menu_items($news_menu->term_id);
    }

    $careers_menu = get_term( $theme_locations['careers-footer-menu'], 'nav_menu' );

    $careers_sub_menus = [];

    if (!is_wp_error($careers_menu)) {
        $careers_sub_menus = wp_get_nav_menu_items($careers_menu->term_id);
    }

    $contact_us_menu = get_term( $theme_locations['contact-us-footer-menu'], 'nav_menu' );

    $contact_us_sub_menus = [];

    if (!is_wp_error($contact_us_menu)) {
        $contact_us_sub_menus = wp_get_nav_menu_items($contact_us_menu->term_id);
    }
?>
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-6 col-lg">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <?php
                        ?>

                        <div class="foo-title">
                            <?php echo $about_us_menu ? $about_us_menu->name : ''; ?>
                        </div>

                        <ul class="foo-list">
                            <?php foreach ($about_us_sub_menus as $menu) { ?>
                                <li><a href="<?php echo $menu->url; ?>"><?php echo $menu->title ?></a></li>
                            <?php } ?>
                            <li class="d-none d-lg-inline-block"><a href="javascript:void(0);">&nbsp;</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="foo-title">
                            <?php echo $our_brands_menu ? $our_brands_menu->name : ''; ?>
                        </div>
                        <ul class="foo-list">
                            <?php foreach ($our_brands_sub_menus as $menu) { ?>
                                <li><a href="<?php echo $menu->url; ?>"><?php echo $menu->title ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="foo-title">
                            <?php echo $sustainability_menu ? $sustainability_menu->name : ''; ?>
                        </div>
                        <ul class="foo-list">
                            <?php foreach ($sustainability_sub_menus as $menu) { ?>
                                <li><a href="<?php echo $menu->url; ?>"><?php echo $menu->title ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-6 col-lg">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="foo-title">
                            <?php echo $news_menu ? $news_menu->name : ''; ?>
                        </div>
                        <ul class="foo-list">
                            <?php foreach ($news_sub_menus as $menu) { ?>
                                <li><a href="<?php echo $menu->url; ?>"><?php echo $menu->title ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="foo-title">
                            <?php echo $careers_menu ? $careers_menu->name : ''; ?>
                        </div>
                        <ul class="foo-list">
                            <?php foreach ($careers_sub_menus as $menu) { ?>
                                <li><a href="<?php echo $menu->url; ?>"><?php echo $menu->title ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="foo-title">
                            <?php echo $contact_us_menu ? $contact_us_menu->name : ''; ?>
                        </div>
                        <ul class="foo-list">
                            <?php foreach ($contact_us_sub_menus as $menu) { ?>
                                <li><a href="<?php echo $menu->url; ?>"><?php echo $menu->title ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb-2 mb-lg-3">
            <div class="col-12">
                <hr class="bordercolor">
            </div>
        </div>

        <div class="row lastrow">
<!--            <div class="col-6 col-md-2 col-lg-auto d-none">-->
<!--                <a href="pdf/terms-of-use.pdf" target="_blank">Terms of Use</a>-->
<!--            </div>-->
            <div class="col-6 col-md-2 col-lg-auto innerboder">
                <a href="<?php echo get_option('privacy_policy'); ?>" target="_blank">Privacy Policy</a>
            </div>
            <div class="col-12 col-md-8 col-lg mt-3 mt-md-0 text-lg-right">
                <?php echo get_option('copyright_text'); ?>
            </div>
        </div>
    </div>
</footer>

<?php echo wp_footer(); ?>

</body>
</html>