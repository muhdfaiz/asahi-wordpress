<?php
/**
 * Footer Navwalker
 *
 */

// Check if Class Exists.
if ( ! class_exists( 'Footer_Navwalker' ) ) :
    class Footer_Navwalker extends Walker_Nav_Menu
    {
        function start_el(&$output, $item, $depth=0, $args=[], $id=0) {
            if ($args->walker->has_children) {
                $output .= "<div class='" .  implode(" ", $item->classes) . "'>";
                $output .= '<div class="foo-title">';
            } else {
                $output .= "<li class='" .  implode(" ", $item->classes) . "'>";
                $output .= '<a href="' . $item->url . '">';
            }

            $output .= $item->title;

            if ($args->walker->has_children) {
                $output .= '</div>';

            } else {
                $output .= '</a>';
            }
        }

        /**
         * Starts the list before the elements are added.
         *
         * @since WP 3.0.0
         *
         * @see Walker_Nav_Menu::start_lvl()
         *
         * @param string           $output Used to append additional content (passed by reference).
         * @param int              $depth  Depth of menu item. Used for padding.
         * @param WP_Nav_Menu_Args $args   An object of wp_nav_menu() arguments.
         */
        public function start_lvl( &$output, $depth = 0, $args = null ) {
            if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
                $t = '';
                $n = '';
            } else {
                $t = "\t";
                $n = "\n";
            }

            $indent = str_repeat( $t, $depth );

            // Default class to add to the file.
            $classes = array( 'foo-list' );

            /**
             * Filters the CSS class(es) applied to a menu list element.
             *
             * @since WP 4.8.0
             *
             * @param array    $classes The CSS classes that are applied to the menu `<ul>` element.
             * @param stdClass $args    An object of `wp_nav_menu()` arguments.
             * @param int      $depth   Depth of menu item. Used for padding.
             */
            $class_names = join( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );
            $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

            $output .= "{$n}{$indent}<ul$class_names>{$n}";
        }

        /**
         * Menu fallback.
         *
         * If this function is assigned to the wp_nav_menu's fallback_cb variable
         * and a menu has not been assigned to the theme location in the WordPress
         * menu manager the function will display nothing to a non-logged in user,
         * and will add a link to the WordPress menu manager if logged in as an admin.
         *
         * @param array $args passed from the wp_nav_menu function.
         * @return string|void String when echo is false.
         */
        public static function fallback( $args ) {
            if ( ! current_user_can( 'edit_theme_options' ) ) {
                return;
            }

            // Initialize var to store fallback html.
            $fallback_output = '';

            // Menu container opening tag.
            $show_container = false;
            if ( $args['container'] ) {
                /**
                 * Filters the list of HTML tags that are valid for use as menu containers.
                 *
                 * @since WP 3.0.0
                 *
                 * @param array $tags The acceptable HTML tags for use as menu containers.
                 *                    Default is array containing 'div' and 'nav'.
                 */
                $allowed_tags = apply_filters( 'wp_nav_menu_container_allowedtags', array( 'div', 'nav' ) );
                if ( is_string( $args['container'] ) && in_array( $args['container'], $allowed_tags, true ) ) {
                    $show_container   = true;
                    $class            = $args['container_class'] ? ' class="menu-fallback-container ' . esc_attr( $args['container_class'] ) . '"' : ' class="menu-fallback-container"';
                    $id               = $args['container_id'] ? ' id="' . esc_attr( $args['container_id'] ) . '"' : '';
                    $fallback_output .= '<' . $args['container'] . $id . $class . '>';
                }
            }

            // The fallback menu.
            $class            = $args['menu_class'] ? ' class="menu-fallback-menu ' . esc_attr( $args['menu_class'] ) . '"' : ' class="menu-fallback-menu"';
            $id               = $args['menu_id'] ? ' id="' . esc_attr( $args['menu_id'] ) . '"' : '';
            $fallback_output .= '<ul' . $id . $class . '>';
            $fallback_output .= '<li class="nav-item"><a href="' . esc_url( admin_url( 'nav-menus.php' ) ) . '" class="nav-link" title="' . esc_attr__( 'Add a menu', 'wp-bootstrap-navwalker' ) . '">' . esc_html__( 'Add a menu', 'wp-bootstrap-navwalker' ) . '</a></li>';
            $fallback_output .= '</ul>';

            // Menu container closing tag.
            if ( $show_container ) {
                $fallback_output .= '</' . $args['container'] . '>';
            }

            // if $args has 'echo' key and it's true echo, otherwise return.
            if ( array_key_exists( 'echo', $args ) && $args['echo'] ) {
                // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                echo $fallback_output;
            } else {
                return $fallback_output;
            }
        }
    }
endif;