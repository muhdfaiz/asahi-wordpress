<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/favicon.ico">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon32x32.png">
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="/apple-touch-icon.png">

    <title><?php the_title('') ?> | <?php echo get_bloginfo( 'name' ); ?></title>
    <meta name="description" content="<?php echo get_option('meta_description'); ?>">

    <?php wp_head() ?>
    <meta property="og:title" content="<?php echo get_option('facebook_og_title'); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo get_home_url(); ?>" />
    <meta property="og:description" content="<?php echo get_option('facebook_og_description'); ?>" />
    <meta property="og:image" content="<?php echo get_option('facebook_og_image'); ?>" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','<?php echo get_option('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo get_option('gtm_id'); ?>"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="topnavigation">
    <div class="continer-fluid">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-xl navbar-light">
                    <?php
                        $custom_logo_id = get_theme_mod( 'custom_logo' );
                        $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );

                        if ( has_custom_logo() ) {
                            echo '<a class="navbar-brand mr-auto" href="/"><img src="' . esc_url( $logo[0] ) . '" alt="logo"></a>';
                        }
                    ?>

                    <div class="row no-gutters d-xl-none">
                        <div class="col-auto ml-auto">
                            <a href="/contact-us" class="bevelcorner-outline d-xl-none">
                                <div class="bevelcorner__inner">Contact Us</div>
                            </a>
                        </div>
                        <div class="col-auto ml-auto px-3">
                            <div class="borderheight d-xl-none"></div>
                        </div>
                        <div class="col-auto ml-auto">
                            <button class="navbar-toggler px-0" type="button" data-toggle="collapse" data-target="#navbarTop" aria-controls="navbarTop" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </div>
                    </div>

                    <?php
                        wp_nav_menu(array(
                            'menu' => 'Header Menu',
                            'depth'           => 2,
                            'container'       => 'div',
                            'container_class' => 'collapse navbar-collapse',
                            'container_id'    => 'navbarTop',
                            'menu_class'      => 'navbar-nav mx-auto pt-4 pb-2 py-xl-0',
                            'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                            'walker'          => new WP_Bootstrap_Navwalker(),
                            'add_li_class'  => 'nav-item pl-0 mx-0 mx-xl-3'
                        ))
                    ?>

                    <div class="adjustmentnav d-none d-xl-block">
                        <div class="row">
                            <div class="col-auto my-auto d-none d-xl-block ml-auto">
                                <a href="/contact-us" class="bevelcorner-outline">
                                    <div class="bevelcorner__inner">Contact Us</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>