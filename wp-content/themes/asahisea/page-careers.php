<?php get_header(); ?>

<?php
$fields = get_field_objects();
$banner = reset($fields['banner']['value'])
?>

    <main>
        <header>
            <div class="brandslider generalslider">
                <div class="brandslider-item bannercareer d-flex align-content-end flex-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 d-lg-none brandslider-placeholder"></div>
                            <div class="col-12 col-lg-8 brandslider-content">
                                <?php if (isset($banner['title']) && $banner['title'] && $banner['title'] !== '#') { ?>
                                    <h2 class="text-yellow mb-0 text-capitalize"><?php echo $banner['title'] ?></h2>
                                <?php } ?>

                                <?php if (isset($banner['description']) && $banner['description'] && $banner['description'] !== '#') { ?>
                                    <p class="text-white bannertxt"><?php echo $banner['description'] ?></p>
                                <?php } ?>

                                <?php if ($banner['button_name'] && $banner['button_name'] !== '#') { ?>
                                    <a href="<?php echo $banner['button_url'] ?: '#' ?>" class="bevelcorner-solid largerpad minwidth mb-4">
                                        <div class="bevelcorner__inner"><?php echo $banner['button_name'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>


        <section class="bg-white sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-9">
                        <?php if (isset($fields['career_page_section_1_title']) && $fields['career_page_section_1_title']) { ?>
                            <p class="text-tiff"><strong><?php echo $fields['career_page_section_1_title']['value'] ?></strong></p>
                        <?php } ?>

                        <?php if (isset($fields['career_page_section_1_description']) && $fields['career_page_section_1_description']) { ?>
                            <p class="sub"><?php echo $fields['career_page_section_1_description']['value'] ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>


        <section id="benefits" class="bg-blue sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-3 mb-lg-4">
                        <?php if (isset($fields['career_page_section_2_title']) && $fields['career_page_section_2_title']) { ?>
                            <h3 class="text-white text-capitalize"><?php echo $fields['career_page_section_2_title']['value'] ?></h3>
                        <?php } ?>
                    </div>

                    <?php if (isset($fields['career_page_section_2_content']) && $fields['career_page_section_2_content']) { ?>
                        <?php foreach ($fields['career_page_section_2_content']['value'] as $key => $item) { ?>
                            <div class="col-12 col-md-6 col-lg-4 my-3 enriching-content d-flex">
                                <div class="d-flex flex-column justify-content-between">
                                    <div>
                                        <img src="<?php echo $item['icon']['url'] ?>" class="mb-3" alt="Attractive Benefits & Compensation">
                                        <h5 class="moblarge" style="color: <?php echo $item['title_color'] ?>"><?php echo $item['title'] ?></h5>
                                        <p class="sub text-white pr-lg-5">
                                            <?php echo $item['description'] ?>
                                        </p>
                                    </div>
                                    <?php if (isset($item['popup_content']) && $item['popup_content']) { ?>
                                        <div>
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#modal<?php echo $key; ?>" class="stretched-link">
                                                <div class="textonly text-white">Find Out More<ion-icon name="chevron-forward-outline" role="img" class="md hydrated" aria-label="chevron forward outline"></ion-icon></div>
                                            </a>
                                        </div>
                                    <?php } ?>
                                </div>

                                <?php if (isset($item['popup_content']) && $item['popup_content']) { ?>
                                    <div class="modal fade generalmodal" id="modal<?php echo $key; ?>" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="close-btn text-blue" data-dismiss="modal"><ion-icon name="close-outline" class="text-blue"></ion-icon></div>
                                                    <?php if (isset($item['title']) && $item['title']) { ?>
                                                        <h4 style="color: <?php echo $item['title_color'] ?>"><?php echo $item['title'] ?></h4>
                                                    <?php } ?>
                                                    <div class="mt-4"><?php echo $item['popup_content']; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>


        <section id="ourculture" class="bg-synergising sect-deskspacer d-flex align-content-lg-center flex-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-8 offset-md-4 offset-lg-0 col-lg-6 offset-lg-6 pt-4 pb-5 py-lg-0 my-auto">
                        <?php if (isset($fields['career_page_section_3_title']) && $fields['career_page_section_3_title']) { ?>
                            <h3 class="text-blue text-capitalize mt-3"><?php echo $fields['career_page_section_3_title']['value'] ?></h3>
                        <?php } ?>

                        <?php if (isset($fields['career_page_section_3_description']) && $fields['career_page_section_3_description']) { ?>
                            <?php echo $fields['career_page_section_3_description']['value'] ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>

        <section id="employee-lifecycle" class="py-5 py-lg-2" style="background: #F6F6F6">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?php if (isset($fields['career_page_section_5_title']) && $fields['career_page_section_5_title']) { ?>
                            <h3 class="text-blue text-capitalize mt-5 mb-3"><?php echo $fields['career_page_section_5_title']['value'] ?></h3>
                        <?php } ?>

                        <?php if (isset($fields['career_page_section_5_description']) && $fields['career_page_section_5_description']) { ?>
                            <?php echo $fields['career_page_section_5_description']['value'] ?>
                        <?php } ?>
                    </div>

                    <div class="col-12 mt-3 mb-4">
                        <?php if (isset($fields['career_page_section_5_image']) && $fields['career_page_section_5_image']) { ?>
                            <img style="width: 88%" src="<?php echo $fields['career_page_section_5_image']['value']['url'] ?>" class="" alt="image">
                        <?php } ?>
                    </div>

                    <?php if (isset($fields['section_5_items']) && $fields['section_5_items']) { ?>
                        <?php foreach ($fields['section_5_items']['value'] as $item) { ?>
                            <div class="col-12 col-md-6 col-lg-4 my-3 enriching-content">
                                <h5 class="moblarge" style="color: <?php echo $item['title_color'] ?>"><?php echo $item['title'] ?></h5>
                                <p class="sub text-white pr-lg-5">
                                    <?php echo $item['description'] ?>
                                </p>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <br>
                </div>
            </div>
        </section>

        <section id="joinus" class="bg-white py-5 py-lg-2">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6 my-auto">
                        <?php if (isset($fields['career_page_section_4_title']) && $fields['career_page_section_4_title']) { ?>
                            <h3 class="text-blue text-capitalize"><?php echo $fields['career_page_section_4_title']['value'] ?></h3>
                        <?php } ?>

                        <?php if (isset($fields['career_page_section_4_description']) && $fields['career_page_section_4_description']) { ?>
                            <p class="tiny sub mb-5"><?php echo $fields['career_page_section_4_description']['value'] ?></p>
                        <?php } ?>

                        <?php if (isset($fields['career_page_section_4_button_name']) && $fields['career_page_section_4_button_name']) { ?>
                            <a href="<?php echo $fields['career_page_section_4_button_url']['value'] ?>" target="_blank" class="bevelcorner-solid largerpad minwidth mb-5">
                                <div class="bevelcorner__inner"><?php echo $fields['career_page_section_4_button_name']['value'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                            </a>
                        <?php } ?>


                    </div>
                    <div class="col-10 col-lg-5 offset-lg-1 mt-n4 my-lg-auto mx-auto pb-4 text-center">
                        <?php if (isset($fields['career_page_section_4_image']) && $fields['career_page_section_4_image']) { ?>
                            <img src="<?php echo $fields['career_page_section_4_image']['value']['url'] ?>" class="careerimgs" alt="image">
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <script>
        document.head.insertAdjacentHTML("beforeend", `<style>
        header .mainslider-item.bannercareer, header .brandslider-item.bannercareer {
            background-image: url(<?php echo $banner['mobile_banner']['url'] ?>);
            background-position: center;
        }
        @media (min-width: 992px) {
            header .mainslider-item.bannercareer, header .brandslider-item.bannercareer {
                background-image: url(<?php echo $banner['desktop_banner']['url'] ?>);
                background-position: center;
            }
        }
        @media all and (min-width: 767px) and (max-width: 991.98px) {
            header .mainslider-item.bannercareer, header .brandslider-item.bannercareer {
                background-image: url(<?php echo $banner['tablet_banner']['url'] ?>);
            }
        }
        <?php if (isset($fields['career_page_section_3_mobile_background_image']) && $fields['career_page_section_3_mobile_background_image']) { ?>
            .bg-synergising {
                background-image: url(<?php echo $fields['career_page_section_3_mobile_background_image']['value']['url'] ?>);
                background-size: cover;
            }
        <?php } ?>

        <?php if (isset($fields['career_page_section_3_desktop_background_image']) && $fields['career_page_section_3_desktop_background_image']) { ?>
            @media all and (min-width: 768px) and (max-width: 991.98px) {
              .bg-synergising {
                background: url(<?php echo $fields['career_page_section_3_desktop_background_image']['value']['url'] ?>) right 46% center no-repeat;
                background-size: cover;
              }
            }
            @media all and (min-width: 992px) {
              .bg-synergising {
                background: url(<?php echo $fields['career_page_section_3_desktop_background_image']['value']['url'] ?>) center no-repeat;
                background-size: cover;
              }
            }
        <?php } ?>
    </style>`)
    </script>
<?php get_footer(); ?>
