<?php get_header(); ?>

<?php
$fields = get_field_objects();
?>

    <main>
        <header>
            <div class="brandslider generalslider">
                <div class="brandslider-item bannersustainability d-flex align-content-end flex-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 d-lg-none brandslider-placeholder"></div>
                            <div class="col-12 col-lg-7 brandslider-content">
                                <?php if (isset($fields['sustainability_post_banner_title']) && $fields['sustainability_post_banner_title'] && $fields['sustainability_post_banner_title'] !== '#') { ?>
                                    <h2 class="text-yellow mb-0 text-capitalize"><?php echo $fields['sustainability_post_banner_title']['value'] ?></h2>
                                <?php } ?>

                                <?php if (isset($fields['sustainability_post_banner_description']) && $fields['sustainability_post_banner_description'] && $fields['sustainability_post_banner_description'] !== '#') { ?>
                                    <p class="text-white bannertxt"><?php echo $fields['sustainability_post_banner_description']['value'] ?></p>
                                <?php } ?>

                                <?php if (isset($fields['sustainability_post_banner_button_name']) && $fields['sustainability_post_banner_button_name']['value']) { ?>
                                    <a href="<?php echo $fields['sustainability_post_banner_button_url']['value'] ?: '#' ?>" class="bevelcorner-solid largerpad minwidth mb-4">
                                        <div class="bevelcorner__inner"><?php echo $fields['sustainability_post_banner_button_name']['value'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="bg-white sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-9">
                        <?php if (isset($fields['sustainability_post_section_1_title']) && $fields['sustainability_post_section_1_title']) { ?>
                            <p class="text-tiff">
                                <strong>
                                    <?php echo $fields['sustainability_post_section_1_title']['value'] ?>
                                </strong>
                            </p>
                        <?php } ?>

                        <?php if (isset($fields['sustainability_post_section_1_description']) && $fields['sustainability_post_section_1_description']) { ?>
                            <p class="sub"><?php echo $fields['sustainability_post_section_1_description']['value'] ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="bg-darkerlight sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-4 pb-lg-1">
                        <?php if (isset($fields['sustainability_post_section_2_title']) && $fields['sustainability_post_section_2_title']) { ?>
                            <h3 class="text-blue text-capitalize"><?php echo $fields['sustainability_post_section_2_title']['value'] ?></h3>
                        <?php } ?>
                    </div>
                </div>

                <div class="row">
                    <?php if (isset($fields['sustainability_post_section_2_items']) && $fields['sustainability_post_section_2_items']) { ?>
                        <?php foreach ($fields['sustainability_post_section_2_items']['value'] as $key => $item) { ?>
                            <?php if (count($fields['sustainability_post_section_2_items']['value']) % 2 === 0) { ?>
                                <div class="col-12 col-md-6 col-lg-5 mt-3 <?php if (($key + 1) > 1 && ($key + 1) % 2 === 0) { echo 'offset-lg-1'; } ?>">
                            <?php } elseif (count($fields['sustainability_post_section_2_items']['value']) % 2 === 0) { ?>
                                <div class="col-12 col-md-6 col-lg-4 mt-3">
                            <?php } else { ?>
                                <div class="col-12 col-md-6 col-lg-4 mt-3">
                            <?php } ?>
                                <img src="<?php echo $item['icon']['url']; ?>" class="icon-h75 mb-3 <?php if (count($fields['sustainability_post_section_2_items']['value']) <= 4) { echo 'desklarger'; } ?>" alt="<?php echo $item['icon']['alt']; ?>">
                                <p class="text-blue sublarger mb-2 <?php if (count($fields['sustainability_post_section_2_items']['value']) <= 4) { echo 'desklarger'; } ?>"><strong><?php echo $item['title']; ?></strong></p>
                                <p class="sub tiny"><?php echo $item['description']; ?></p>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>


        <section class="bg-white sect-spacer sustainable">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-4 pb-lg-1">
                        <?php if (isset($fields['sustainability_post_section_3_title']) && $fields['sustainability_post_section_3_title']) { ?>
                            <h3 class="text-blue text-capitalize"><?php echo $fields['sustainability_post_section_3_title']['value'] ?></h3>
                        <?php } ?>

                        <?php if (isset($fields['sustainability_post_section_3_description']) && $fields['sustainability_post_section_3_description']) { ?>
                            <p class="sub"><?php echo $fields['sustainability_post_section_3_description']['value'] ?></p>
                        <?php } ?>
                    </div>


                    <?php if (isset($fields['sustainability_post_section_3_items']) && $fields['sustainability_post_section_3_items']) { ?>
                        <?php foreach ($fields['sustainability_post_section_3_items']['value'] as $item) { ?>
                            <div class="col-12 col-lg col-md-6 mb-md-4 mb-lg-0">
                                <div class="card sustainable-cover">
                                    <img class="card-img" src="<?php echo $item['image']['url']; ?>" alt="images">
                                    <div class="card-img-overlay">
                                        <div class="sustainable-caption">
                                            <p class="suplarger text-white mb-0"><strong><?php echo $item['title']; ?></strong></p>
                                            <p class="sub suptiny text-white"><?php echo $item['description']; ?></p>
                                            <div class="textonly text-white">Know More <ion-icon name="chevron-forward-outline"></ion-icon></div>
                                        </div>
                                    </div>
                                    <a href="<?php echo $item['link']; ?>" class="stretched-link"></a>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>

    </main>

<script>
	document.head.insertAdjacentHTML("beforeend", `<style>
       header .mainslider-item.bannersustainability, header .brandslider-item.bannersustainability {
            background-image: url(<?php echo isset($fields['sustainability_post_banner_mobile']) ? $fields['sustainability_post_banner_mobile']['value']['url'] : '' ?>);
            background-position: center;
        }
        @media (min-width: 992px) {
            header .mainslider-item.bannersustainability, header .brandslider-item.bannersustainability {
                background-image: url(<?php echo isset($fields['sustainability_post_banner_desktop']) ? $fields['sustainability_post_banner_desktop']['value']['url'] : '' ?>);
                background-position: center;
            }
        }
    </style>`)
</script>
<?php get_footer(); ?>