<?php get_header(); ?>

<?php
$fields = get_field_objects();
$banner = reset($fields['banner']['value'])
?>

    <main>
        <header>
            <div class="brandslider generalslider">
                <div class="brandslider-item bannerwhoweare d-flex align-content-end flex-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 d-lg-none brandslider-placeholder"></div>
                            <div class="col-12 col-lg-8 brandslider-content">
                                <?php if (isset($banner['title']) && $banner['title'] && $banner['title'] !== '#') { ?>
                                    <h2 class="text-yellow mb-0 text-capitalize"><?php echo $banner['title'] ?></h2>
                                <?php } ?>

                                <?php if (isset($banner['description']) && $banner['description'] && $banner['description'] !== '#') { ?>
                                    <p class="text-white bannertxt"><?php echo $banner['description'] ?></p>
                                <?php } ?>

                                <?php if ($banner['button_name'] && $banner['button_name'] !== '#') { ?>
                                    <a href="<?php echo $banner['button_url'] ?: '#' ?>" class="bevelcorner-solid largerpad minwidth mb-4">
                                        <div class="bevelcorner__inner"><?php echo $banner['button_name'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="bg-white pt-3 pt-lg-2 pb-0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-md-6 py-2">
                        <div class="bevelboxcover bg-lightbrown singletopleft h-100">
                            <div class="row justify-content-center">
                                <div class="col-12 col-lg-2 order-lg-2 text-lg-right">
                                    <?php if (isset($fields['about_us_page_vision_icon']) && $fields['about_us_page_vision_icon']) { ?>
                                        <img src="<?php echo $fields['about_us_page_vision_icon']['value']['url'] ?>" class="mt-lg-n2 mb-2 mr-lg-n3 icon-h70" alt="Icon Vision">
                                    <?php } ?>
                                </div>
                                <div class="col-12 col-lg-8 order-lg-1">
                                    <?php if (isset($fields['about_us_page_vision_title']) && $fields['about_us_page_vision_title']) { ?>
                                        <h2 class="text-blue mb-2 tiny"><?php echo $fields['about_us_page_vision_title']['value'] ?></h2>
                                    <?php } ?>
                                    <?php if (isset($fields['about_us_page_vision_description']) && $fields['about_us_page_vision_description']) { ?>
                                        <p class="sub tiny text-dark mb-0"><?php echo $fields['about_us_page_vision_description']['value'] ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 py-2">
                        <div class="bevelboxcover bg-lightbrown singlebtmright h-100">
                            <div class="row justify-content-center">
                                <div class="col-12 col-lg-2 order-lg-2 text-lg-right">
                                    <?php if (isset($fields['about_us_page_mission_icon']) && $fields['about_us_page_mission_icon']) { ?>
                                        <img src="<?php echo $fields['about_us_page_mission_icon']['value']['url'] ?>" class="mt-lg-n2 mb-2 mr-lg-n3 icon-h70" alt="Icon Vision">
                                    <?php } ?>
                                </div>
                                <div class="col-12 col-lg-8 order-lg-1">
                                    <?php if (isset($fields['about_us_page_mission_title']) && $fields['about_us_page_mission_title']) { ?>
                                        <h2 class="text-blue mb-2 tiny"><?php echo $fields['about_us_page_mission_title']['value'] ?></h2>
                                    <?php } ?>
                                    <?php if (isset($fields['about_us_page_mission_description']) && $fields['about_us_page_mission_description']) { ?>
                                        <p class="sub tiny text-dark mb-0"><?php echo $fields['about_us_page_mission_description']['value'] ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="bg-white sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <?php if (isset($fields['about_us_page_our_story_title']) && $fields['about_us_page_our_story_title']) { ?>
                            <h3 class="text-blue text-capitalize mb-3"><?php echo $fields['about_us_page_our_story_title']['value'] ?></h3>
                        <?php } ?>
                    </div>
                    <div class="col-12 col-lg-8">
                        <?php if (isset($fields['about_us_page_our_story_content']) && $fields['about_us_page_our_story_content']) { ?>
                            <?php foreach ($fields['about_us_page_our_story_content']['value'] as $item) { ?>
                                <p class="sub mb-4 pb-2">
                                    <strong class="text-tiff d-block"><?php echo $item['title'] ?></strong>
                                    <?php echo $item['description'] ?>
                                </p>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-4">
                        <?php if (isset($fields['about_us_page_what_we_do_title']) && $fields['about_us_page_what_we_do_title']) { ?>
                            <h3 class="text-blue text-capitalize mb-3"><?php echo $fields['about_us_page_what_we_do_title']['value'] ?></h3>
                        <?php } ?>
                    </div>
                    <div class="col-12 col-lg-8">
                        <?php if (isset($fields['about_us_page_what_we_do_content']) && $fields['about_us_page_what_we_do_content']) { ?>
                            <?php foreach ($fields['about_us_page_what_we_do_content']['value'] as $item) { ?>
                                <p class="sub mb-4 pb-2">
                                    <strong class="text-tiff d-block"><?php echo $item['title'] ?></strong>
                                    <?php echo $item['description'] ?>
                                </p>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>


        <section class="bg-blue-sky sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-4 pb-lg-1">
                        <?php if (isset($fields['about_us_page_our_product_title']) && $fields['about_us_page_our_product_title']) { ?>
                            <h3 class="text-blue text-capitalize"><?php echo $fields['about_us_page_our_product_title']['value'] ?></h3>
                        <?php } ?>
                    </div>

                    <?php if (isset($fields['about_us_page_our_product_images']) && $fields['about_us_page_our_product_images']) { ?>
                        <?php foreach ($fields['about_us_page_our_product_images']['value'] as $item) { ?>

                            <div class="col-12 col-md-5 my-2">
                                <img src="<?php echo $item['image']['url'] ?>" class="w-100 shape-radius" alt="<?php echo $item['image']['alt'] ?>">
                            </div>
                        <?php } ?>
                    <?php } ?>

                    <div class="col-12 col-lg-10 mt-4">
                        <?php if (isset($fields['about_us_page_our_product_content']) && $fields['about_us_page_our_product_content']) { ?>
                            <?php foreach ($fields['about_us_page_our_product_content']['value'] as $key => $item) { ?>
                                <p class="sub <?php if ($key !== 0 ) { echo 'pt-2'; } ?>">
                                    <strong class="text-blue d-block"><?php echo $item['title'] ?></strong>
                                    <?php echo $item['description'] ?>
                                </p>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 mt-5 mb-4 pb-lg-1">
                        <?php if (isset($fields['about_us_page_our_plant_title']) && $fields['about_us_page_our_plant_title']) { ?>
                            <h3 class="text-blue text-capitalize"><?php echo $fields['about_us_page_our_plant_title']['value'] ?></h3>
                        <?php } ?>
                    </div>

                    <?php if (isset($fields['about_us_page_our_plant_images']) && $fields['about_us_page_our_plant_images']) { ?>
                        <?php foreach ($fields['about_us_page_our_plant_images']['value'] as $item) { ?>
                            <div class="col-12 col-md-5 my-2">
                                <img src="<?php echo $item['image']['url'] ?>" class="w-100 shape-radius" alt="<?php echo $item['image']['alt'] ?>">
                            </div>
                        <?php } ?>
                    <?php } ?>

                    <div class="col-12 col-lg-10 mt-4">
                        <?php if (isset($fields['about_us_page_our_plant_content']) && $fields['about_us_page_our_plant_content']) { ?>
                            <?php foreach ($fields['about_us_page_our_plant_content']['value'] as $key => $item) { ?>
                                <p class="sub <?php if ($key !== 0 ) { echo 'pt-2'; } ?>">
                                    <strong class="text-blue d-block"><?php echo $item['title'] ?></strong>
                                    <?php echo $item['description'] ?>
                                </p>

                                <?php if (isset($item['items']) && $item['items']) { ?>
                                    <ul class="sub tiny general-check">
                                        <?php foreach ($item['items'] as $key => $item) { ?>
                                            <li><?php echo $item['name']; ?></li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>



        <section class="bg-lightbrown sect-spacer">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-12 col-lg-4 mb-2 mb-lg-5">
                        <?php if (isset($fields['about_us_page_maintaining_highest_standards_title']) && $fields['about_us_page_maintaining_highest_standards_title']) { ?>
                            <h3 class="text-blue text-capitalize pr-lg-4">
                                <?php echo $fields['about_us_page_maintaining_highest_standards_title']['value'] ?>
                            </h3>
                        <?php } ?>
                    </div>
                    <div class="col-12 col-lg-6 mb-4 mb-lg-0">
                        <?php if (isset($fields['about_us_page_maintaining_highest_standards_description']) && $fields['about_us_page_maintaining_highest_standards_description']) { ?>
                            <p class="sub"><?php echo $fields['about_us_page_maintaining_highest_standards_description']['value'] ?></p>
                        <?php } ?>
                    </div>
                </div>

                <div class="row">
                    <?php if (isset($fields['about_us_page_maintaining_highest_standards_content']) && $fields['about_us_page_maintaining_highest_standards_content']) { ?>
                        <?php foreach ($fields['about_us_page_maintaining_highest_standards_content']['value'] as $key => $item) { ?>
                            <div class="col-12 col-md-6 col-lg-4 mt-3">
                                <img src="<?php echo $item['icon']['url'] ?>" class="icon-h70 mb-3" alt="icon">
                                <h5 class="text-blue"><?php echo $item['title'] ?></h5>
                                <p class="sub tiny"><?php echo $item['description'] ?></p>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>



        <section class="bg-light sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-4 mb-lg-5">
                        <?php if (isset($fields['about_us_page_our_assurance_title']) && $fields['about_us_page_our_assurance_title']) { ?>
                            <h3 class="text-blue text-capitalize"><?php echo $fields['about_us_page_our_assurance_title']['value'] ?></h3>
                        <?php } ?>
                    </div>

                    <div class="col-12 px-3 px-lg-0">
                        <div class="slide4items generalslider slideralignleft">
                            <?php if (isset($fields['about_us_page_our_assurance_image']) && $fields['about_us_page_our_assurance_image']) { ?>
                                <?php foreach ($fields['about_us_page_our_assurance_image']['value'] as $key => $item) { ?>
                                    <div class="ourbrands-item">
                                        <div class="mx-1 mx-lg-3 mb-5 position-relative mb-lg-0">
                                            <div class="brands-logoimg">
                                                <img src="<?php echo $item['image']['url'] ?>" alt="<?php echo $item['image']['alt'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <!--if more than 5 need to change the class .mb-lg-0 to .mb-lg-5-->
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <section class="bg-white sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-4 mb-lg-5">
                        <?php if (isset($fields['about_us_page_leadership_quote_title']) && $fields['about_us_page_leadership_quote_title']) { ?>
                            <h3 class="text-blue text-capitalize"><?php echo $fields['about_us_page_leadership_quote_title']['value'] ?></h3>
                        <?php } ?>
                    </div>

                    <div class="col-10 col-lg-4 groupceo">
                        <?php if (isset($fields['about_us_page_leadership_quote_image']) && $fields['about_us_page_leadership_quote_image']) { ?>
                            <img src="<?php echo $fields['about_us_page_leadership_quote_image']['value']['url'] ?>" alt="<?php echo $fields['about_us_page_leadership_quote_image']['value']['alt'] ?>">
                        <?php } ?>
                    </div>
                    <div class="col-12 col-lg-7 my-auto">
                        <?php if (isset($fields['about_us_page_leadership_quote_sentence_heading']) && $fields['about_us_page_leadership_quote_sentence_heading']) { ?>
                            <h4 class="text-blue mb-3"><?php echo $fields['about_us_page_leadership_quote_sentence_heading']['value'] ?></h4>
                        <?php } ?>

                        <div class="row">
                            <div class="col-12"><img src="<?php echo get_template_directory_uri()  . '/img/quote-open.svg'; ?>" alt="icon" class="icon-h35"></div>
                            <div class="col-12 pt-4 pb-2">
                                <?php if (isset($fields['about_us_page_leadership_quote_sentence_summary']) && $fields['about_us_page_leadership_quote_sentence_summary']) { ?>
                                    <p><?php echo $fields['about_us_page_leadership_quote_sentence_summary']['value'] ?></p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="text-blue mb-1">
                                    <?php if (isset($fields['about_us_page_leadership_quote_person_name']) && $fields['about_us_page_leadership_quote_person_name']) { ?>
                                        <strong><?php echo $fields['about_us_page_leadership_quote_person_name']['value'] ?></strong>
                                    <?php } ?>
                                </p>
                                <a href="javascript:void(0);" class="textonly" data-toggle="modal" data-target="#ourgroupceo">Read More <ion-icon name="chevron-down-outline"></ion-icon></a>
                            </div>
                            <div class="col-auto text-right"><img src="<?php echo get_template_directory_uri()  . '/img/quote-close.svg'; ?>" alt="icon" class="icon-h35"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bg-blue-sky sect-spacer leadership">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-7 mb-2">
                        <?php if (isset($fields['about_us_page_leadership_team_title']) && $fields['about_us_page_leadership_team_title']) { ?>
                            <h3 class="text-blue text-capitalize"><?php echo $fields['about_us_page_leadership_team_title']['value'] ?></h3>
                        <?php } ?>
                        <?php if (isset($fields['about_us_page_leadership_team_description']) && $fields['about_us_page_leadership_team_description']) { ?>
                            <p class="sub"><?php echo $fields['about_us_page_leadership_team_description']['value'] ?></p>
                        <?php } ?>
                    </div>
                    <div class="col-12 mb-5">
                        <?php if (isset($fields['about_us_page_leadership_team_bannner']) && $fields['about_us_page_leadership_team_bannner']) { ?>
                            <img class="mt-3" style="width: 100%;" src="<?php echo $fields['about_us_page_leadership_team_bannner']['value']['url'] ?>">
                        <?php } ?>
                    </div>
                    <div class="w-100"></div>

                    <?php if (isset($fields['about_us_page_leadership_team_persons']) && $fields['about_us_page_leadership_team_persons']) { ?>
                        <?php foreach ($fields['about_us_page_leadership_team_persons']['value'] as $key => $item) { ?>
                            <div class="col-12 col-md-6 col-lg-4 leadership-cover">
                                <div class="row">
                                    <img src="<?php echo $item['image']['url']; ?>" class="leadership-thumbnail" alt="<?php echo $item['image']['alt']; ?>">
                                    <div class="leadership-caption">
                                        <p class="sublarger mb-0"><strong><?php echo $item['name']; ?></strong></p>
                                        <p class="tiny"><?php echo $item['position']; ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>


        <!-- Modal -->
        <div class="modal fade generalmodal" id="ourgroupceo" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="close-btn text-blue" data-dismiss="modal"><ion-icon name="close-outline" class="text-blue"></ion-icon></div>
                        <?php if (isset($fields['about_us_page_leadership_quote_sentence']) && $fields['about_us_page_leadership_quote_sentence']) { ?>
                            <h3 class="text-blue text-capitalize"><?php echo $fields['about_us_page_leadership_quote_sentence']['value'] ?></h3>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <script>
		document.head.insertAdjacentHTML("beforeend", `<style>
        header .mainslider-item.bannerwhoweare, header .brandslider-item.bannerwhoweare {
            background-image: url(<?php echo $banner['mobile_banner']['url'] ?>);
            background-position: center;
        }
        @media (min-width: 992px) {
            header .mainslider-item.bannerwhoweare, header .brandslider-item.bannerwhoweare {
                background-image: url(<?php echo $banner['desktop_banner']['url'] ?>);
                background-position: center;
            }
        }
        @media all and (min-width: 767px) and (max-width: 991.98px) {
            header .mainslider-item.bannerwhoweare, header .brandslider-item.bannerwhoweare {
                background-image: url(<?php echo $banner['tablet_banner']['url'] ?>);
            }
        }
    </style>`)
    </script>
<?php get_footer(); ?>
