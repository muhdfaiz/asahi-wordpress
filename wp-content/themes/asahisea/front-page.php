<?php get_header(); ?>

<?php
$fields = get_field_objects();
?>

<main>
    <header>
        <div class="mainslider generalslider">
            <?php foreach ($fields['banner']['value'] as $key => $banner) { ?>
                <div class="hidden-lg-up mainslider-item slider0<?php echo $key + 1; ?> d-flex align-content-end flex-wrap" onclick="location.href = '<?php echo $banner['button_url'] ?: '#' ?>';" style="cursor: pointer;">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 d-lg-none mainslider-placeholder"></div>
                            <div class="col-12 col-lg-6 mainslider-content">

                                <h2 class="mb-0 text-capitalize" style="color: <?php echo $banner['title_color'] ?>"><?php echo $banner['title'] ?></h2>
                                <p class="bannertxt" style="color: <?php echo $banner['description_color'] ?>"><?php echo $banner['description'] ?></p>

                                <?php if ($banner['button_url'] && $banner['button_name']) { ?>
                                	<a href="<?php echo $banner['button_url'] ?: '#' ?>" class="bevelcorner-solid largerpad minwidth">
                                    	<div class="bevelcorner__inner"><?php echo $banner['button_name'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                	</a>
                                <?php } ?>
                            	</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </header>

    <section class="bg-white sect-spacer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <h3 class="text-blue text-capitalize mt-lg-5"><?php echo $fields['who_title']['value'] ?></h3>
                    <p class="sub pr-lg-5 mb-4"><strong><?php echo $fields['who_summary']['value'] ?></strong></p>

                    <p class="sub pr-lg-5 mb-4"><?php echo $fields['who_description']['value'] ?></p>

                    <a href="<?php echo $fields['who_button_1_url']['value'] ?>" class="bevelcorner-solid largerpad minwidth mr-lg-2 mb-2">
                        <div class="bevelcorner__inner"><?php echo $fields['who_button_1_name']['value'] ?><ion-icon name="chevron-forward-sharp"></ion-icon></div>
                    </a>

                    <a href="<?php echo $fields['who_button_2_url']['value']['url'] ?>" target="_blank" class="bevelcorner-solid largerpad minwidthsuplg mb-2">
                        <div class="bevelcorner__inner"><?php echo $fields['who_button_2_name']['value'] ?> <ion-icon name="chevron-forward-sharp" role="img" class="md hydrated" aria-label="chevron forward sharp"></ion-icon></div>
                    </a>
                </div>
                <div class="col-12 col-lg-6 text-center">
                    <img src="<?php echo $fields['who_image']['value']['url'] ?>" class="w-100 whowerareimg px-lg-2 px-xl-5" alt="<?php echo $fields['who_image']['value']['alt'] ?>">
                </div>
            </div>
        </div>
    </section>

    <section class="bg-blue sect-spacer">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3 mb-lg-5">
                    <h3 class="text-white text-capitalize"><?php echo $fields['enrich_title']['value'] ?></h3>
                </div>

                <?php foreach ($fields['enrich_items']['value'] as $item) { ?>
                    <div class="col-12 col-md-6 my-3 enriching-content">
                        <img src="<?php echo $item['image']['url'] ?>" class="mb-3" alt="<?php echo $item['image']['value']['alt'] ?>">
                        <h5 class="moblarge" style="color: <?php echo $item['title_color'] ?>"><?php echo $item['title'] ?></h5>
                        <p class="sub text-white pr-lg-5">
                            <?php echo $item['description'] ?>
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>


    <section class="bg-light sect-spacer">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-4 mb-lg-5">
                    <h3 class="text-blue text-capitalize"><?php echo $fields['brand_title']['value'] ?></h3>
                </div>

                <div class="col-12 px-2 px-lg-0">
                    <div class="ourbrands generalslider">
                        <?php foreach ($fields['brands']['value'] as $item) { ?>
                            <div class="ourbrands-item">
                                <div class="mx-2 mx-lg-3 position-relative">
                                    <div class="brands-logoimg">
                                        <img src="<?php echo $item['brand_image']['url'] ?>" alt="<?php echo $item['brand_name'] ?>">
                                    </div>
                                    <a href="<?php echo $item['brand_url'] ?>" class="stretched-link"></a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="col-12 text-right">
                    <a href="/our-brands" class="mx-2 mx-lg-3 text-blue-light nostyle text-capitalize"><strong>View all brands <ion-icon name="chevron-forward-sharp" class="ml-2 mr-n1"></ion-icon></strong></a>
                </div>

            </div>
        </div>
    </section>


    <section class="bg-homereshaping sect-tabletspacer d-flex align-content-lg-center flex-wrap">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 px-0">
                    <img src="<?php echo $fields['reshaping_background_image_mobile']['value']['url'] ?>" class="d-lg-none w-100" alt="<?php echo $fields['reshaping_background_image_mobile']['value']['alt'] ?>">
                </div>
                <div class="col-12 col-lg-6 pt-4 pb-5 py-lg-0 my-auto">
                    <img src="<?php echo $fields['reshaping_icon']['value']['url'] ?>" class="iconimgs" alt="<?php echo $fields['reshaping_icon']['value']['alt'] ?>">
                    <h3 class="text-tiff text-capitalize mt-3"><?php echo $fields['reshaping_title']['value'] ?></h3>
                    <p class="text-white sub mb-4 pb-lg-2"><?php echo $fields['reshaping_description']['value'] ?></p>

                    <?php foreach ($fields['reshaping_buttons']['value'] as $key => $reshaping_button) { ?>
                        <?php if ($key === 0) {
                            $reshaping_class = 'mr-2 mr-lg-3';
                        } else {
                            $reshaping_class = '';
                        } ?>
                        <a href="<?php echo $reshaping_button['reshaping_button_url'] ?>" class="bevelcorner-solid minwidthlg mb-2 <?php echo $reshaping_class ?>">
                            <div class="bevelcorner__inner"><?php echo $reshaping_button['reshaping_button_name'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>


    <section class="bg-homeculture sect-tabletspacer d-flex align-content-lg-center flex-wrap">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 order-lg-6 px-0">
                    <img src="<?php echo $fields['our_culture_background_image_mobile']['value']['url'] ?>" class="d-lg-none w-100" alt="<?php echo $fields['our_culture_background_image_mobile']['value']['alt'] ?>">
                </div>
                <div class="col-12 col-lg-6 pt-4 pb-5 py-lg-0 px-lg-4 my-auto">
                    <img src="<?php echo $fields['our_culture_icon']['value']['url'] ?>" class="iconimgs" alt="<?php echo $fields['our_culture_icon']['value']['url'] ?>">
                    <h3 class="text-yellow text-capitalize mt-3"><?php echo $fields['our_culture_title']['value'] ?></h3>
                    <p class="text-white sub mb-4 pb-lg-2"><?php echo $fields['our_culture_description']['value'] ?>
                        <br><br>
                        <strong><?php echo $fields['our_culture_person_name']['value'] ?></strong></p>


                    <a href="<?php echo $fields['our_culture_button_url']['value']['url'] ?>" class="bevelcorner-solid minwidthlg mb-2">
                        <div class="bevelcorner__inner">Find Out More <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                    </a>
                </div>
            </div>
        </div>
    </section>

</main>

    <script>
		document.head.insertAdjacentHTML("beforeend", `<style>
        <?php foreach ($fields['banner']['value'] as $key => $banner) { ?>
            <?php $bannerNumber = $key + 1; ?>
            header .mainslider-item.slider0<?php echo $bannerNumber; ?> {
              background-image: url(<?php echo $banner['mobile_banner']['url'] ?>);
            }
            @media (min-width: 992px) {
              header .mainslider-item.slider0<?php echo $bannerNumber; ?> {
                background-image: url(<?php echo $banner['desktop_banner']['url'] ?>);
              }
            }
            @media all and (min-width: 767px) and (max-width: 991.98px) {
              header .mainslider-item.slider0<?php echo $bannerNumber; ?> {
                background-image: url(<?php echo $banner['tablet_banner']['url'] ?>);
              }
            }
            @media all and (min-width: 560px) and (max-width: 766.98px) {
              header .mainslider-item.slider0<?php echo $bannerNumber; ?> {
                background-image: url(<?php echo $banner['desktop_banner']['url'] ?>);
              }
            }
        <?php } ?>
        <?php if (isset($fields['reshaping_background_image_desktop']) && $fields['reshaping_background_image_desktop']) { ?>
            @media (min-width: 992px) {
                .bg-homereshaping {
                    background: url(<?php echo $fields['reshaping_background_image_desktop']['value']['url'] ?>) center no-repeat;
                    background-size: cover;
                }
            }
        <?php } ?>

        <?php if (isset($fields['our_culture_background_image_desktop']) && $fields['our_culture_background_image_desktop']) { ?>
            @media all and (min-width: 992px) {
              .bg-homeculture {
                background: url(<?php echo $fields['our_culture_background_image_desktop']['value']['url'] ?>) center no-repeat;
                background-size: cover;
              }
            }
        <?php } ?>
    </style>`)
    </script>

<?php get_footer(); ?>
