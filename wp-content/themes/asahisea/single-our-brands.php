<?php get_header(); ?>

<?php
$fields = get_field_objects();
?>

    <main>
        <header>
            <div class="brandslider generalslider">
                <?php if (isset($fields['our_brands_post_banners']) && $fields['our_brands_post_banners']) { ?>
                    <?php foreach ($fields['our_brands_post_banners']['value'] as $key => $banner) { ?>
                        <div class="brandslider-item bannerourbrands<?php echo $key + 1; ?>  d-flex align-content-lg-end flex-wrap">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-lg-6 brandslider-content">
                                        <h2 style="color: <?php echo $banner['title_color']; ?> !important;"><?php echo $banner['title'] ?></h2>
                                        <p class="bannertxt" style="color: <?php echo $banner['description_color']; ?> !important;"><?php echo $banner['description'] ?></p>

                                        <?php if (isset($banner['button_name']) && $banner['button_name']) { ?>
                                            <a href="<?php echo $banner['button_url'] ?: '#' ?>" class="bevelcorner-solid largerpad minwidth mb-4">
                                                <div class="bevelcorner__inner"><?php echo $banner['button_name'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                            </a>
                                        <?php } ?>
                                    </div>
                                    <div class="col-12 d-lg-none brandslider-placeholdersmaller"></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </header>


        <section class="bg-blue sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="brands-logoimg2 thumblogo"></div>
                        <!-- <div class="brands-logoimg">
                          <img src="../img/logo-wonda.png" alt="wonda">
                        </div> -->
                    </div>
                    <div class="col-12 col-lg-6 order-lg-1 my-auto pb-4 pt-1 pt-lg-0 pb-lg-0 brands-content">
                        <?php if (isset($fields['our_brands_post_brand_text_1']) && $fields['our_brands_post_brand_text_1']) { ?>
                            <p class="text-white larger"><strong><?php echo $fields['our_brands_post_brand_text_1']['value'] ?></strong></p>
                        <?php } ?>

                        <?php if (isset($fields['our_brands_post_brand_text_2']) && $fields['our_brands_post_brand_text_2']) { ?>
                            <p class="text-white larger"><?php echo $fields['our_brands_post_brand_text_2']['value'] ?></p>
                        <?php } ?>

                        <?php if (isset($fields['our_brands_post_additional_brand_image']) && $fields['our_brands_post_additional_brand_image']) { ?>
                            <img src="<?php echo $fields['our_brands_post_additional_brand_image']['value']['url'] ?>" class="w-100 shape-radius" alt="<?php echo $fields['our_brands_post_additional_brand_image']['value']['url'] ?>">
                        <?php } ?>
                    </div>
                </div>

            </div>
        </section>


        <section class="bg-light sect-spacer slideroverhide">
            <div class="container">
                <?php if ((isset($fields['our_brands_post_product_title']) && $fields['our_brands_post_product_title']) || isset($fields['our_brands_post_product_description']) && $fields['our_brands_post_product_description']) { ?>
                    <div class="row">
                        <div class="col-12 col-lg-9">
                            <?php if (isset($fields['our_brands_post_product_title']) && $fields['our_brands_post_product_title']) { ?>
                                <h3 class="text-blue"><?php echo $fields['our_brands_post_product_title']['value'] ?></h3>
                            <?php } ?>

                            <?php if (isset($fields['our_brands_post_product_description']) && $fields['our_brands_post_product_description']) { ?>
                                <p class="sub pr-lg-5 mb-4"><?php echo $fields['our_brands_post_product_description']['value'] ?></p>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>

                <div class="row justify-content-around">
                    <div class="col-10 col-md-11 col-lg-12 my-4 order-2 order-lg-1">
                        <ul class="titlebrandslider productnavslider-1 generalslider">
                            <?php if (isset($fields['our_brands_post_products']) && $fields['our_brands_post_products']) { ?>
                                <?php foreach ($fields['our_brands_post_products']['value'] as $key => $item) { ?>
                                    <li class="titlebrandslider-item">
                                        <div data-slide="<?php echo $key + 1; ?>" class="titlecover <?php echo $key === 0 ? 'active' : ''; ?>"><span><?php echo $item['title']; ?></span></div>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>


                    <div class="col-10 col-lg-4 my-auto order-1 order-lg-2">
                        <div class="productdetails productcontentslider-1">
                            <?php if (isset($fields['our_brands_post_products']) && $fields['our_brands_post_products']) { ?>
                                <?php foreach ($fields['our_brands_post_products']['value'] as $key => $item) { ?>
                                    <div class="productdetails-item">
                                        <div class="productimgs">
                                            <img src="<?php echo $item['image']['url']; ?>" alt="<?php echo $item['image']['alt']; ?>" class="w-100">
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 my-auto order-3">
                        <div class="productdetails productcontentslider-1">
                            <?php if (isset($fields['our_brands_post_products']) && $fields['our_brands_post_products']) { ?>
                                <?php foreach ($fields['our_brands_post_products']['value'] as $key => $item) { ?>
                                    <div class="productdetails-item">
                                        <h4 class="text-blue"><?php echo $item['name']; ?></h4>
                                        <p class="sub mb-4 pb-1 pb-lg-0 mb-lg-5"><?php echo $item['description']; ?></p>

                                        <p class="bannersublg text-blue mb-2 pb-1"><strong>Available Sizes</strong></p>
                                        <ul class="availablesizes">
                                            <?php if (isset($item['size']) && $item['size']) { ?>
                                                <?php foreach ($item['size'] as $size) { ?>
                                                    <li><?php echo $size['name']; ?></li>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php if ((isset($fields['our_brands_post_product_2_title']) && $fields['our_brands_post_product_2_title']['value']) || (isset($fields['our_brands_post_product_2_description']) && $fields['our_brands_post_product_2_description']['value']) ||
            isset($fields['our_brands_post_products_2']) && $fields['our_brands_post_products_2']['value'] ) { ?>
            <section class="bg-blue-sky sect-spacer slideroverhide">
                <div class="container">
                    <?php if ((isset($fields['our_brands_post_product_2_title']) && $fields['our_brands_post_product_2_title']) || isset($fields['our_brands_post_product_2_description']) && $fields['our_brands_post_product_2_description']) { ?>
                        <div class="row">
                            <div class="col-12 col-lg-9">
                                <?php if (isset($fields['our_brands_post_product_2_title']) && $fields['our_brands_post_product_2_title']) { ?>
                                    <h3 class="text-blue"><?php echo $fields['our_brands_post_product_2_title']['value'] ?></h3>
                                <?php } ?>

                                <?php if (isset($fields['our_brands_post_product_2_description']) && $fields['our_brands_post_product_2_description']) { ?>
                                    <p class="sub pr-lg-5 mb-4"><?php echo $fields['our_brands_post_product_2_description']['value'] ?></p>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if (isset($fields['our_brands_post_products_2']) && isset($fields['our_brands_post_products_2']['value']) && $fields['our_brands_post_products_2']['value']) { ?>
                        <div class="row justify-content-around">
                            <div class="col-10 col-md-11 col-lg-12 my-4 order-2 order-lg-1">
                                <ul class="titlebrandslider productnavslider-1 generalslider">
                                    <?php foreach ($fields['our_brands_post_products_2']['value'] as $key => $item) { ?>
                                        <li class="titlebrandslider-item">
                                            <div data-slide="<?php echo $key + 1; ?>" class="titlecover <?php echo $key === 0 ? 'active' : ''; ?>"><span><?php echo $item['title']; ?></span></div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                    <?php } ?>

                    <?php if (isset($fields['our_brands_post_products_2']) && isset($fields['our_brands_post_products_2']['value']) && $fields['our_brands_post_products_2']['value']) { ?>
                        <div class="col-10 col-lg-4 my-auto order-1 order-lg-2">
                            <div class="productdetails productcontentslider-1">
                                <?php foreach ($fields['our_brands_post_products_2']['value'] as $key => $item) { ?>
                                    <div class="productdetails-item">
                                        <div class="productimgs">
                                            <img src="<?php echo $item['image']['url']; ?>" alt="<?php echo $item['image']['alt']; ?>" class="w-100">
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if (isset($fields['our_brands_post_products_2']) && isset($fields['our_brands_post_products_2']['value']) && $fields['our_brands_post_products_2']['value']) { ?>
                        <div class="col-12 col-lg-6 my-auto order-3">
                            <div class="productdetails productcontentslider-1">
                                <?php foreach ($fields['our_brands_post_products_2']['value'] as $key => $item) { ?>
                                    <div class="productdetails-item">
                                        <h4 class="text-blue"><?php echo $item['name']; ?></h4>
                                        <p class="sub mb-4 pb-1 pb-lg-0 mb-lg-5"><?php echo $item['description']; ?></p>

                                        <p class="bannersublg text-blue mb-2 pb-1"><strong>Available Sizes</strong></p>
                                        <ul class="availablesizes">
                                            <?php if (isset($item['size']) && $item['size']) { ?>
                                                <?php foreach ($item['size'] as $size) { ?>
                                                    <li><?php echo $size['name']; ?></li>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>


                    <?php if (isset($fields['our_brands_post_product_2_description']) && $fields['our_brands_post_product_2_description']) { ?>
                        <div class="row">
                            <div class="col-12 mt-5">
                                <p class="suptiny"><?php echo $fields['our_brands_post_product_2_note']['value'] ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </section>
        <?php } ?>

        <section class="bg-white sect-spacer moblarge slideroverhide">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?php if (isset($fields['our_brands_post_you_may_also_like_title']) && $fields['our_brands_post_you_may_also_like_title']) { ?>
                            <h3 class="text-blue mb-4"><?php echo $fields['our_brands_post_you_may_also_like_title']['value'] ?></h3>
                        <?php } ?>
                    </div>

                    <div class="col-10 col-lg-12 mx-auto text-left justify-content-start">
                        <div class="crossellslider generalslider">
                            <?php if (isset($fields['our_brands_post_you_may_also_like_products']) && $fields['our_brands_post_you_may_also_like_products']) { ?>
                                <?php foreach ($fields['our_brands_post_you_may_also_like_products']['value'] as $key => $item) { ?>
                                    <div class="crossellslider-item">
                                        <div class="xsellproduct">
                                            <div class="productimgs">
                                                <img src="<?php echo $item['image']['url']; ?>" alt="<?php echo $item['image']['alt']; ?>" class="w-100">
                                            </div>
                                            <p class="text-blue"><?php echo $item['name']; ?></p>

                                            <a href="<?php echo $item['link']; ?>" class="bevelcorner-solid largerpad minwidth">
                                                <div class="bevelcorner__inner">Know More <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                            </a>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <script>
		document.head.insertAdjacentHTML("beforeend", `<style>
		<?php foreach ($fields['our_brands_post_banners']['value'] as $key => $banner) { ?>
            header .mainslider-item.bannerourbrands<?php echo $key + 1; ?>, header .brandslider-item.bannerourbrands<?php echo $key + 1; ?> {
                background-image: url(<?php echo $banner['mobile_banner']['url'] ?>);
                background-position: center;
            }
            @media (min-width: 992px) {
                header .mainslider-item.bannerourbrands<?php echo $key + 1; ?>, header .brandslider-item.bannerourbrands<?php echo $key + 1; ?> {
                    background-image: url(<?php echo $banner['desktop_banner']['url'] ?>);
                }
            }

            <?php if (isset($banner['tablet_banner']) && $banner['tablet_banner']) { ?>
                @media all and (min-width: 560px) and (max-width: 991.98px) {
                    header .mainslider-item.bannerourbrands<?php echo $key + 1; ?>,
                    header .brandslider-item.bannerourbrands<?php echo $key + 1; ?> {
                    background-image: url(<?php echo $banner['tablet_banner']['url'] ?>);
                    }
                }
            <?php } ?>
       <?php } ?>
       .brands-logoimg2.thumblogo {
            background: url(<?php echo $fields['our_brands_post_brand_logo']['value']['url'] ?>) center no-repeat;
            background-size: cover;
            height: 160px;
        }
        @media (min-width: 321px) {
            .brands-logoimg2.thumblogo {
                height: 185px;
            }
        }
        @media (min-width: 414px) {
            .brands-logoimg2.thumblogo {
                height: 200px;
            }
        }
        @media (min-width: 768px) {
            .brands-logoimg2.thumblogo {
                height: 230px;
            }
        }
    </style>`)
    </script>
<?php get_footer(); ?>