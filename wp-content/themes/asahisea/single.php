<?php get_header(); ?>

<main>
    <section class="bg-white sect-spacer sect_news">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <a href="/" class="bevelcorner-solid largerpad minwidth mb-4">
                        <div class="bevelcorner__inner lefticon"><ion-icon name="chevron-back-sharp" class="lefticon"></ion-icon> Return</div>
                    </a>

                    <h3 class="text-blue text-capitalize"><?php echo the_title(); ?></h3>
                    <?php echo the_content() ?>
                </div>
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>