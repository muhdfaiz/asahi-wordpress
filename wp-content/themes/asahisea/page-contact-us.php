<?php get_header(); ?>

<?php
$fields = get_field_objects();
$banner = reset($fields['banner']['value'])
?>

<main>
    <header>
        <div class="brandslider generalslider">
            <div class="brandslider-item bannercontactus d-flex align-content-end flex-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-12 d-lg-none brandslider-placeholder"></div>
                        <div class="col-12 col-lg-6 brandslider-content">
                            <?php if (isset($banner['title']) && $banner['title'] && $banner['title'] !== '#') { ?>
                                <h2 class="text-yellow mb-0 text-capitalize"><?php echo $banner['title'] ?></h2>
                            <?php } ?>

                            <?php if (isset($banner['description']) && $banner['description'] && $banner['description'] !== '#') { ?>
                                <p class="text-white bannertxt"><?php echo $banner['description'] ?></p>
                            <?php } ?>

                            <?php if ($banner['button_name'] && $banner['button_name'] !== '#') { ?>
                                <a href="<?php echo $banner['button_url'] ?: '#' ?>" class="bevelcorner-solid largerpad minwidth mb-4">
                                    <div class="bevelcorner__inner"><?php echo $banner['button_name'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section id="findushere" class="bg-white sect-spacer">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-4">
                    <h3 class="text-blue"><?php echo $fields['contact_page_office_section_title']['value'] ?></h3>
                </div>

                <div class="col-12 col-lg-8 my-3">
                    <div class="bevelboxcover h-100">
                        <div class="row">
                            <div class="col-12 col-lg-6 pr-3 pr-lg-5 pb-3 pb-lg-0">
                                <img src="<?php echo $fields['contact_page_malaysia_icon']['value']['url'] ?>" alt="Malaysia"><br>

                                <p class="larger pr-4 mt-3"><strong><?php echo $fields['contact_page_malaysia_office_address_title']['value'] ?></strong></p>
                                <p class="tiny sub"><strong>Address:</strong><?php echo $fields['contact_page_malaysia_office_address']['value'] ?></p>
                                <p class="tiny sub"><strong>Tel:</strong><?php echo $fields['contact_page_malaysia_office_phone_number']['value'] ?></p>
                            </div>

                            <div class="col-12 col-lg-6 pr-3 pr-lg-3 pl-lg-5 pt-4 pt-lg-0 linebetween">
                                <img src="<?php echo $fields['contact_page_international_icon']['value']['url'] ?>" alt="Malaysia"><br>

                                <p class="larger pr-4 mt-3"><strong><?php echo $fields['contact_page_international_office_address_title']['value'] ?></strong></p>
                                <p class="tiny sub"><strong>Address:</strong><?php echo $fields['contact_page_international_office_address']['value'] ?></p>
                                <p class="tiny sub"><strong>Tel:</strong><?php echo $fields['contact_page_international_office_phone_number']['value'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4 my-3">
                    <div class="bevelboxcover h-100">
                        <img src="<?php echo $fields['contact_page_vietnam_icon']['value']['url'] ?>" alt="Malaysia"><br>

                        <p class="larger pr-4 mt-3"><strong><?php echo $fields['contact_page_vietnam_office_address_title']['value'] ?></strong></p>
                        <p class="tiny sub"><strong>Address:</strong><?php echo $fields['contact_page_vietnam_office_address']['value'] ?></p>
                        <p class="tiny sub"><strong>Tel:</strong><?php echo $fields['contact_page_vietnam_office_phone_number']['value'] ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="getintouch" class="bg-contactus sect-spacer">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-12 col-lg-6">
                    <div class="row no-gutters">
                        <div class="col-12">
                            <h3 class="text-blue"><?php echo $fields['contact_page_inquiry_title']['value'] ?></h3>
                            <p class="larger sub mb-4"><?php echo $fields['contact_page_inquiry_description']['value'] ?></p>
                        </div>

                        <?php echo the_content() ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade generalmodal" id="modalthankyou" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body py-4 my-2 text-center">
                    <h5 class="text-blue">Thank you for getting in touch!</h5>
                    <p class="sub tiny">We’ve received your inquiry and will get back to you as soon as possible</p>
                    <a href="javascript:void(0);" data-dismiss="modal" class="bevelcorner-solid largerpad minwidth mt-3">
                        <div class="bevelcorner__inner">Got it! <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
	document.getElementById('recipient-email').value = '<?php echo $fields['contact_page_general_inquiry_email']['value'] ?>';

	document.getElementById("inquiry-type").onchange = function(event){
		if (event.target.value === 'Exports Related') {
			document.getElementById('recipient-email').value = '<?php echo $fields['contact_page_export_inquiry_email']['value'] ?>';
			document.getElementById('subject').value = 'Export Enquiry';
		}

		if (event.target.value === 'General Inquiries') {
			document.getElementById('recipient-email').value = '<?php echo $fields['contact_page_general_inquiry_email']['value'] ?>';
			document.getElementById('subject').value = 'General Enquiry';
		}

		if (event.target.value === 'Career Related') {
			document.getElementById('recipient-email').value = '<?php echo $fields['contact_page_career_inquiry_email']['value'] ?>';
			document.getElementById('subject').value = 'Career Enquiry';
		}
	};

    document.head.insertAdjacentHTML("beforeend", `<style>
        .wpcf7-response-output {
            display:none !important;
        }
        .wpcf7-not-valid-tip {
            margin-top: 5px;
            font-weight: 500;
            color: #EA3019;
            font-size: calc(15px / var(--global-font-size));
        }
        .wp-block-contact-form-7-contact-form-selector {
            width: 100% !important;
        }
        header .mainslider-item.bannercontactus, header .brandslider-item.bannercontactus {
            background-image: url(<?php echo $banner['mobile_banner']['url'] ?>);
            background-position: center;
        }
        @media (min-width: 992px) {
            header .mainslider-item.bannercontactus, header .brandslider-item.bannercontactus {
                background-image: url(<?php echo $banner['desktop_banner']['url'] ?>);
                background-position: center;
            }
        }
        @media all and (min-width: 767px) and (max-width: 991.98px) {
            header .mainslider-item.bannercontactus, header .brandslider-item.bannercontactus {
                background-image: url(<?php echo $banner['tablet_banner']['url'] ?>);
            }
        }
        .bg-contactus {
            background-image: url(<?php echo $fields['contact_page_inquiry_background_image_mobile']['value']['url'] ?>);
            background-position: center;
        }
        @media (min-width: 768px) {
            .bg-contactus {
                background-image: url(<?php echo $fields['contact_page_inquiry_background_image_desktop']['value']['url'] ?>);
                background-position: center;
            }
        }
    </style>`)
</script>

<?php get_footer(); ?>
