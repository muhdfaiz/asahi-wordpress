<?php get_header(); ?>

<?php
$fields = get_field_objects();
?>

<main>
    <header>
        <div class="brandslider generalslider">
            <div class="brandslider-item bannernewsinnergoodday d-flex align-content-end flex-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-12 d-lg-none mainslider-placeholder"></div>
                        <div class="col-12 col-lg-6 mainslider-content">
                            <h2 class="mb-0 text-capitalize" style="color: <?php echo $fields['new_image_title_color']['value'] ?>"><?php echo $fields['news_image_title']['value'] ?></h2>
                            <p class="bannertxt" style="color: <?php echo $fields['news_image_description_color']['value'] ?>"><?php echo $fields['news_image_description']['value'] ?></p>

                            <?php if (isset($fields['news_button_name']) && $fields['news_button_name']['value']) { ?>
                                <a href="<?php echo $fields['news_button_url']['value'] ?: '#' ?>" class="bevelcorner-solid largerpad minwidth mb-4">
                                    <div class="bevelcorner__inner"><?php echo $fields['news_button_name']['value'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="bg-white sect-spacer sect_news">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <a href="/news" class="bevelcorner-solid largerpad minwidth mb-4">
                        <div class="bevelcorner__inner lefticon"><ion-icon name="chevron-back-sharp" class="lefticon"></ion-icon> Return</div>
                    </a>

                    <h3 class="text-blue text-capitalize"><?php echo the_title(); ?></h3>
                    <?php echo the_content() ?>
                </div>
            </div>
        </div>
    </section>
</main>

<script>
	document.head.insertAdjacentHTML("beforeend", `<style>
       header .mainslider-item.bannernewsinnergoodday, header .brandslider-item.bannernewsinnergoodday {
            background-image: url(<?php echo isset($fields['news_image_mobile']) ? $fields['news_image_mobile']['value']['url'] : '' ?>);
            background-position: center;
        }
        @media (min-width: 992px) {
            header .mainslider-item.bannernewsinnergoodday, header .brandslider-item.bannernewsinnergoodday {
                background-image: url(<?php echo isset($fields['news_image_desktop']) ? $fields['news_image_desktop']['value']['url'] : '' ?>);
                background-position: center;
            }
        }
    </style>`)
</script>
<?php get_footer(); ?>