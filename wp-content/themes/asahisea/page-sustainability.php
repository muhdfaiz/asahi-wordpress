<?php get_header(); ?>

<?php
$fields = get_field_objects();
$banner = reset($fields['banner']['value'])
?>

    <main>
        <header>
            <div class="brandslider generalslider">
                <div class="brandslider-item bannersustainability d-flex align-content-end flex-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 d-lg-none brandslider-placeholder"></div>
                            <div class="col-12 col-lg-6 brandslider-content">
                                <?php if (isset($banner['title']) && $banner['title'] && $banner['title'] !== '#') { ?>
                                    <h2 class="text-yellow mb-0 text-capitalize"><?php echo $banner['title'] ?></h2>
                                <?php } ?>

                                <?php if (isset($banner['description']) && $banner['description'] && $banner['description'] !== '#') { ?>
                                    <p class="text-white bannertxt"><?php echo $banner['description'] ?></p>
                                <?php } ?>

                                <?php if ($banner['button_name'] && $banner['button_name'] !== '#') { ?>
                                    <a href="<?php echo $banner['button_url'] ?: '#' ?>" class="bevelcorner-solid largerpad minwidth mb-4">
                                        <div class="bevelcorner__inner"><?php echo $banner['button_name'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="bg-white sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-9">
                        <?php if (isset($fields['sustainability_page_title']) && $fields['sustainability_page_title']) { ?>
                            <p class="text-tiff"><strong><?php echo $fields['sustainability_page_title']['value'] ?></strong></p>
                        <?php } ?>

                        <?php if (isset($fields['sustainability_page_description']) && $fields['sustainability_page_description']) { ?>
                            <p class="sub"><?php echo $fields['sustainability_page_description']['value'] ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="bg-blue sect-spacer sustainable">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-4 pb-lg-1">
                        <?php if (isset($fields['sustainability_page_sustainability_targets_title']) && $fields['sustainability_page_sustainability_targets_title']) { ?>
                            <h3 class="text-white text-capitalize"><?php echo $fields['sustainability_page_sustainability_targets_title']['value'] ?></h3>
                        <?php } ?>
                    </div>

                    <?php if (isset($fields['sustainability_page_sustainability_targets_items']) && $fields['sustainability_page_sustainability_targets_items']) { ?>
                        <?php foreach ($fields['sustainability_page_sustainability_targets_items']['value'] as $item) { ?>
                            <div class="col-12 col-md-6 mb-md-4 mb-lg-0 col-lg">
                                <div class="card sustainable-cover">
                                    <img class="card-img d-none d-lg-block" src="<?php echo $item['desktop_image']['url'] ?>" alt="<?php echo $item['desktop_image']['alt'] ?>">
                                    <img class="card-img d-block d-lg-none" src="<?php echo $item['mobile_image']['url'] ?>" alt="<?php echo $item['mobile_image']['alt'] ?>">
                                    <div class="card-img-overlay">
                                        <div class="sustainable-caption">
                                            <p class="larger text-white mb-0"><strong><?php echo $item['title'] ?></strong></p>
                                            <p class="sub suptiny text-white"><?php echo $item['description'] ?></p>
                                            <div class="textonly text-white">Know More <ion-icon name="chevron-forward-outline"></ion-icon></div>
                                        </div>
                                    </div>
                                    <a href="<?php echo $item['url']; ?>"" class="stretched-link"></a>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>

        <section id="stayupdate" class="bg-light sect-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-4 pb-lg-1">
                        <?php if (isset($fields['sustainability_page_stay_updated_title']) && $fields['sustainability_page_stay_updated_title']) { ?>
                            <h3 class="text-blue text-capitalize"><?php echo $fields['sustainability_page_stay_updated_title']['value'] ?></h3>
                        <?php } ?>

                        <?php if (isset($fields['sustainability_page_stay_updated_description']) && $fields['sustainability_page_stay_updated_description']) { ?>
                            <p class="sub"><?php echo $fields['sustainability_page_stay_updated_description']['value'] ?></p>
                        <?php } ?>
                    </div>

                    <?php if (isset($fields['sustainability_page_stay_updated_items']) && $fields['sustainability_page_stay_updated_items']) { ?>
                        <?php foreach ($fields['sustainability_page_stay_updated_items']['value'] as $key => $item) { ?>
                            <div class="col-12 col-md-6 mb-md-4 mb-lg-0 col-lg">
                                <div class="card sustainable-cover">
                                    <img class="card-img" src="<?php echo $item['image']['url']; ?>" alt="<?php echo $item['image']['alt']; ?>">
                                    <div class="card-img-overlay">
                                        <div class="sustainable-caption">
                                            <p class="suplarger text-white mb-0"><strong><?php echo $item['title']; ?></strong></p>
                                            <p class="sub suptiny text-white"><?php echo $item['description']; ?></p>
                                            <div class="textonly text-white"><?php echo $item['button_name']; ?> <ion-icon name="chevron-forward-outline"></ion-icon></div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#modal<?php echo $key; ?>" class="stretched-link"></a>
                                </div>

                                <div class="modal fade generalmodal" id="modal<?php echo $key; ?>" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="close-btn text-blue" data-dismiss="modal"><ion-icon name="close-outline" class="text-blue"></ion-icon></div>
                                                <h4 class="text-blue"><?php echo $item['popup_title']; ?></h4>
                                                <?php echo $item['popup_content']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>
    </main>

    <script>
		document.head.insertAdjacentHTML("beforeend", `<style>
        header .mainslider-item.bannersustainability, header .brandslider-item.bannersustainability {
            background-image: url(<?php echo $banner['mobile_banner']['url'] ?>);
            background-position: center;
        }
        @media (min-width: 992px) {
            header .mainslider-item.bannersustainability, header .brandslider-item.bannersustainability {
                background-image: url(<?php echo $banner['desktop_banner']['url'] ?>);
                background-position: center;
            }
        }
        @media all and (min-width: 767px) and (max-width: 991.98px) {
            header .mainslider-item.bannersustainability, header .brandslider-item.bannersustainability {
                background-image: url(<?php echo $banner['tablet_banner']['url'] ?>);
            }
        }
    </style>`)
    </script>
<?php get_footer(); ?>
