<?php get_header(); ?>

<?php
$fields = get_field_objects();

$args = array(
        'post_type' => 'news',
        'post_status' => 'publish',
        'posts_per_page' => 8,
        'orderby' => 'publish_date',
        'order' => 'DESC',
    );

$loop = new WP_Query( $args );

?>



<main>
    <header>
        <div class="brandslider generalslider">
            <?php foreach ($fields['banner']['value'] as $key => $banner) { ?>
                <div class="brandslider-item bannernews d-flex align-content-end flex-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 d-lg-none brandslider-placeholder"></div>
                            <div class="col-12 col-lg-6 brandslider-content">
                                <h2 class="text-yellow mb-0 text-capitalize"><?php echo $banner['title'] ?></h2>
                                <p class="text-white bannertxt"><?php echo $banner['description'] ?></p>

                                <?php if ($banner['button_name'] && $banner['button_name'] !== '#') { ?>
                                    <a href="<?php echo $banner['button_url'] ?: '#' ?>" class="bevelcorner-solid largerpad minwidth mb-4">
                                        <div class="bevelcorner__inner"><?php echo $banner['button_name'] ?> <ion-icon name="chevron-forward-sharp"></ion-icon></div>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </header>

    <section id="lateststories" class="bg-blue-bright sect-spacer">
        <div class="container">
            <div class="row mb-4 pb-lg-1 pt-3">
                <div class="col-12 col-lg-6 col-xl-8 my-auto">
                    <h3 class="text-blue text-capitalize mb-4 mb-lg-0">Our latest Stories</h3>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="card-group newslisting w-100">
                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <?php
                            $post_id = get_the_ID();
                            $thumbnail_image = get_field('news_thumbnail_image', $post_id);
                            $tags = !empty($tags = get_tags()) ? $tags[0] : '';
                        ?>

                        <div class="col-12 col-md-8 col-lg-4 mb-4">
                            <div class="card h-100">
                                <img class="card-img-top" src="<?php echo $thumbnail_image['url'] ?>" alt="Goodday Virtual Launch in the Philippines Thumbnail">

                                <div class="card-header row no-gutters">
                                    <?php if ($tags) { ?>
                                        <div class="col-auto my-auto">
                                            <span class="badge badge-primary"><?php echo $tags->name ?></span>
                                        </div>
                                    <?php } ?>
                                    <div class="col text-right my-auto date"><?php echo get_the_date( 'jS M Y' ); ?></div>
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title text-blue"><?php echo the_title(); ?></h5>
                                    <p class="card-text"><?php echo get_the_excerpt(); ?></p>
                                </div>
                                <div class="card-footer">
                                    <a href="<?php echo get_permalink($post_id) ?>" class="stretched-link">Read More <ion-icon name="chevron-forward-sharp"></ion-icon></a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>

                    <?php echo wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </section>
</main>

<script>
document.head.insertAdjacentHTML("beforeend", `<style>
    header .mainslider-item.bannerourbrands, header .brandslider-item.bannernews {
        background-image: url(<?php echo $banner['mobile_banner']['url'] ?>);
        background-position: center;
    }
    @media (min-width: 992px) {
        header .mainslider-item.bannerourbrands, header .brandslider-item.bannernews {
            background-image: url(<?php echo $banner['desktop_banner']['url'] ?>);
            background-position: center;
        }
    }
    @media all and (min-width: 767px) and (max-width: 991.98px) {
        header .mainslider-item.bannerourbrands, header .brandslider-item.bannernews {
            background-image: url(<?php echo $banner['tablet_banner']['url'] ?>);
        }
    }
</style>`)
</script>

<?php get_footer(); ?>
